# The Walkie Gang - Dashboard

[Angular](http://Angular.io) based web-application for 'The Walkie Gang'. By using this 
web-application user have the possibility to manage database entries,
language files, support tickets or other important functions with an ease.
 
This application communicates with the [Firebase](https://firebase.google.com) realtime database
and is hosted on a development server running on an expressJS
Framework developed by [Sebastian Enns](https://www.sebenns.com).

You can visit a deployed production build of this web-application on [twg.sebenns.com](https://twg.sebenns.com).
Whenever the master branch gets updated a newer version will be deployed.

You can find a wider documentation for >everything< [here](https://git.thm.de/senn19/thewalkiegang-backend/wikis/Home).

## Installation

If you just want to get a small insight into this project you can visit a deployed
version of this project on [twg.sebenns.com](https://twg.sebenns.com). 

### Installation: Developer (local version)

First of all you have to install all required technologies listed in the section below. 
Install [NodeJS (v10.16.3 or higher)](https://nodejs.org/en/), which will provide 
[npm Package Manager (v6.12.0 or higher)](https://nodejs.org/en/) as well.

Afterwards navigate to the root directory of your cloned project, run `npm install`
and `npm run i-client` to install all necessary `node_modules` listed in the `package.json` of your client and your server. Don't forget to compile all typescript files in the `server` folder. You can 
use the TypeScript FileWatcher if you need so (`tsc -p server`).

You can configure your configuration file (`server/config.json`) in the server folder to edit 
your port if necessary so. If you are finished you can run `npm run webStart` to start your
expressJS server, which will be located at `localhost:3535` on default and host the Angular web-application. 

Now you need to run `cd client && ng build (--prod)` to get a valid angular build or run `npm run proxy` in the client
directory to just 'ng serve' with a proxy connection to the expressJS Framework (api request handling).

You can find a detailed documentation for developers [here](https://git.thm.de/senn19/thewalkiegang-backend/wikis/installation-guide).


## Fundamental Modeling Concept

![image](http://sebenns.com/src/fmc-wgbackend.png)

**Briefly explained**: The client submits a request to the express server. Due to the Angular Guard **Meta** all necessary configurations for meta data, page settings and cache settings are first retrieved from the database and processed. The language constants are then replaced by the **Language Service**, which requests a mapped version of the 'standard language' from the express server. Only then is the 'frontend' displayed to the client, which also has a direct connection to the generic **database controller**. If the client now wants to call the admin control panel, it must first authenticate itself. The **Admin** Guard also checks whether the user has the appropriate access rights for certain backend components.

## Technologies

This project had been created with the expressJS Framework developed by 
[Sebastian Enns](https://www.sebenns.com) and provides a modular expressJS
server as well as an [Angular](http://angular.io) module, which contains an
empty installation of a web-application in the `client` folder.

The web-application is build on this module and communicates with  
[Firebase](https://Firebase.google.com). **Continuous Integration** and **Continuous Deployment** 
will be provided by gitlab-ci, which will deploy a production build on each new commit in the master branch. 

The deployed version is hosted on a Linux Debian server which uses 
Apache as its web-server. This Apache-server has a special configuration to listen
on specific ports and provides a nodeJS server on specific dns-configurations.
For example [twg.sebenns.com](https://twg.sebenns.com) listens on localhost:3535.

Detailed documentation for interactions between technologies is available [here](https://git.thm.de/senn19/thewalkiegang-backend/wikis/software-architecture).  

### List of required technologies:

> NodeJS: Latest LTS Version: 10.16.3 (includes npm 6.9.0)

- [NodeJS (v10.16.3 or higher)](https://nodejs.org/en/)
- [npm Package Manager (v6.12.0 or higher)](https://nodejs.org/en/)

#### List of used technologies/frameworks:

| client-side | server-side |
| :-----------: | :-----------: |
| [Angular](http://angular.io) | [Firebase](https://Firebase.google.com) |
| | [expressJS Framework](https://sebenns.com) |

#### List of used libraries/APIs:

- [AngularFire2 / Firebase API](https://github.com/angular/angularfire2)

## SEO: Dynamic Rendering

**Problem in a nutshell:** The processing of JavaScript is currently difficult and not all search engine crawlers can process JavaScript immediately or successfully. Especially since search engines do not execute JavaScript code for efficiency reasons and therefore crawl only the slim non rendered version of the Single Page Application.

![image](http://sebenns.com/src/crawlerdiff.png) 

On the left you can see how crawlers would see the source code **without** prerendering the page. On the right you can see how crawlers will see the source code after prerendering the application with a client-side-rendering solution.

For dynamic rendering, the Web server must recognize crawlers - for example, by checking the user agent. Requests from crawlers are forwarded to a renderer, requests from users are processed. If required, the dynamic renderer provides a version of the content suitable for the crawler, such as a static HTML version. You can activate the dynamic renderer for all pages or page by page.

![image](http://sebenns.com/src/googleImage.png)

To be able to perform this process on the client side, the CSR (client-side-rendering) solution from Google is used, which uses the [Prerender.IO API](https://prerender.io). It would also have been possible to use the SSR (server-side-rendering) solution from Angular (Angular Universal), but the binding to the expressJS framework would not have been possible then. 

A further documentation will be available [here](https://git.thm.de/senn19/thewalkiegang-backend/wikis/software-architecture#dynamic-rendering).

## Document Structure

The project is default structured by expressJS framework and folders will be defined as followed:

- **client**: all necessary files for client
    - **dist**: web-application builds
    - **node_modules**: necessary client-modules
    - **projects**: created angular applications
        - **acp**: 'Dashboard' app folder
            - **e2e**: end to end testing folder
            - **src**: source folder for this application
                - **app**: source code of application
                    - **includes**: partials (modules/components)
                    - **modules**: contains splitted angular modules for lazy loading
                    - **pages**: application/angular based pages/components
                - **assets**: images, icons and other asset stuff
                    - **scss**: stylesheets and variables
                - **environments**: settings for whole environment
                - **guards**: route protection classes (guards)
                - **services**: application/angular based services for CRUD/Auth/...
                - **utils**: visual design documents for this application
                - **interfaces**: classes, types and interfaces for data structures
- **node_modules**: npm/nodeJS dependency folder
- **server**: all necessary file for server
    - **interfaces**: expressJS Framework interfaces, like Loader/Module
    - **modules**: expressJS Framework modules, drag-and-drop installation

More information and a wider documentation are available [here](https://git.thm.de/senn19/thewalkiegang-backend/wikis/software-architecture).

## Services & Database Structure

Current available services and apis:

- **LanguageService** +  **Api**: Returns a json/map full of language strings - allows to switch between different languages.
- **AuthManager**: Manages authentications like OAuth or simple login processes.
- **AlertManager**: Manages alert boxes and displays important messages (can be customized).
- **ConfigManager**: Manages configuration settings and allows to update them as fast as possible (extended DbController).
- **DatabaseController**: Contains all necessary database methods for create, read, update and delete generic items.
- **PermManager**: Manages permissions and allows to update them as fast as possible (caching included).

For further information about the structure of services, database and other important classes you have
to visit the full documentation [here](https://git.thm.de/senn19/thewalkiegang-backend/wikis/api-references).
