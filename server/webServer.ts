import * as express from 'express';
import {Express} from 'express';
import {ModLoader} from './ModLoader';

require('./globalDecs');
require('./overrides');

// Load all installed modules
ModLoader.loadFiles(ModLoader.modulesPath);

// Initialize core modules in priority queue
ModLoader.onEvent(ModLoader.events.init);

declare const $cfg;
const app: Express = express(),
      api: Express = express();

app.use(express.json());
app.use(require('prerender-node').set('prerenderToken', 'sgWHyJruaQtHLrSKXIfH'));

// Load additional functionality before routing
ModLoader.onEvent(ModLoader.events.load, app);


// Load different routes of different modules in priority queue
ModLoader.onEvent(ModLoader.events.route, app);

// Start listening on port, set by process env
app.listen($cfg.get('SERVER_PORT'), () =>
{
    console.log(`
    
    -------------------------------------------------------------
                     ${$cfg.get('PROJECT_NAME')} is running
    -------------------------------------------------------------
    Found Modules: ${Object.keys(ModLoader.getModuleList())}
    -------------------------------------------------------------
    
    `);
});

app.use('/api', api);

// Additional method to load anything after server has been started (api routes).
ModLoader.onEvent(ModLoader.events.start, api);

// This will be the last event which will get triggered, perfect for fallback routing.
ModLoader.onEvent(ModLoader.events.last, app);
