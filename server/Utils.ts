export class Utils
{
    /**
     * Merges two objects in recursive order, without overriding left object values
     * @param  left   object, which will get all values but wont be overwritten in values
     * @param  right  object, which will be merged in left object
     * @returns  {any}  merged left object will be returned
     */
    public static mergeObjs(left, right): any
    {
        for (const key in right)
        {
            // Check if right obj really has that key
            if (!right.hasOwnProperty(key))
            {
                continue;
            }

            // Check if left obj has that key, and add it if not
            if (!left.hasOwnProperty(key))
            {
                left[key] = right[key];
            }

            // Left obj has this key but the value is unknown
            if (typeof left[key] !== 'object')
            {
                continue;
            }

            // Left obj has this key and the value is typeof object
            left[key] = this.mergeObjs(left[key], right[key]);
        }

        // return merged object
        return left;
    }

    /**
     * Removes all not in right object existent keys in left object in recursive order
     * @param  left   object, which will be cleaned up
     * @param  right  object, which will be checked on
     * @returns  {any}  cleaned up object will be returned
     */
    public static cleanUpObjs(left, right): any
    {
        for (const key in left)
        {
            // Check if left obj really has that key
            if (!left.hasOwnProperty(key))
            {
                continue;
            }

            // skip if a key starts with _.
            if (key.startsWith('_'))
            {
                continue;
            }

            // Check if right obj hast that key, if not, delete it in left obj
            if (!right.hasOwnProperty(key))
            {
                delete left[key];
                continue;
            }

            // Left and right obj have that key, if it is a string value, skip it
            if (typeof left[key] !== 'object')
            {
                continue;
            }

            // If not, repeat that check in object of key
            left[key] = this.cleanUpObjs(left[key], right[key]);
        }

        return left;
    }
}
