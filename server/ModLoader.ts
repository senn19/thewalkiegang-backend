import {Loader} from './interfaces/Loader';

export class ModLoader extends Loader
{
    public static modulesPath = `${__dirname}/modules/**/*.mod.js`;
    public static events = {
        init : 'onInit',
        load : 'onLoad',
        route: 'onRouting',
        start: 'onServerStart',
        last : 'onFallback'
    };

    /**
     * Returns current list of modules
     * @returns  any moduleList
     */
    public static getModuleList(): any
    {
        return this.fileList;
    }
}
