export class Config {

    private cfg: Map<string, string> = new Map();

    /**
     *  Returns a conf variable specified by key or the config map on empty key
     * @returns  {string|Map<string, string>} config variable on specified key | map on key === null
     */
    public get(key: string = null): string | Map<string, string>
    {
        if (key === null)
        {
            return this.cfg;
        }

        return this.cfg.get(key);
    }

    /**
     * Sets a configuration variable with specified key and value
     * @param  {string} key    variable key to store
     * @param  {string} value  value of specified key
     */
    public set(key: string, value: string): void
    {
        this.cfg.set(key, value);
    }
}
