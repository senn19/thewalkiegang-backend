import {ConfManager} from './ConfManager';
import {Module, priority} from '../../interfaces/Module';

export class ConfManagerMod implements Module
{
    public priority = priority.core;

    public onInit(): void
    {
        ConfManager.createConfigFile();
        console.warn('Configuration file has been (re)created.\nYou can edit it in the root directory of your server folder.');
    }

    public onLoad(): void
    {
        // Store configuration instance in global variable
        ConfManager.loadConfigVariables();
        (global as any).$cfg = ConfManager.getConfig();
    }
}
