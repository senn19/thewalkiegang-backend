import {Config} from './Config';

declare const $utils;
declare const __basePath;

export class ConfManager
{
    private static configPath = `${__basePath}/config.json`;
    private static config: Config = new Config();

    /**
     * Loads config files from all modules and write them
     * into one configuration file in root directory
     */
    public static createConfigFile(): void
    {
        // Define glob, path and get all config files in modules folder
        const glob    = require('glob'),
              fs      = require('fs'),
              configs = glob.sync(`${__basePath}/modules/**/config.json`);

        let configFile = {};

        // Iterate through config files in modules
        for (const cfg of configs)
        {
            // Fill empty configFile with values from first iteration
            if (Object.keys(configFile).length === 0)
            {
                configFile = JSON.parse(fs.readFileSync(cfg, 'utf-8'));
                continue;
            }

            // Merge configs into one object
            configFile = $utils.mergeObjs(configFile, JSON.parse(fs.readFileSync(cfg, 'utf-8')));
        }

        // Check if config file already exists, merge it with new configs and clean it up.
        if (fs.existsSync(this.configPath))
        {
            const temp = $utils.mergeObjs(JSON.parse(fs.readFileSync(this.configPath, 'utf-8')), configFile);
            configFile = $utils.cleanUpObjs(temp, configFile);
        }

        fs.writeFileSync(this.configPath, JSON.stringify(configFile, null, 4));
    }

    /**
     * Returns config instance
     * @returns {Config} Instance with configuration map
     */
    public static getConfig(): Config
    {
        return this.config;
    }

    /**
     * Retrieves all config variables from given ini file.
     */
    public static loadConfigVariables(): void | never
    {
        const fs = require('fs');

        // Check if given file exists, otherwise throw an exception
        if (!fs.existsSync(this.configPath))
        {
            console.error(`No such file or directory: ${this.configPath}`);
            process.exit(1);
        }

        // Reads whole file and splits @\n
        const contents: {} = JSON.parse(fs.readFileSync(this.configPath, 'utf-8'));

        // Read every line, split on assign and map it into attribute
        for (const category of Object.values(contents))
        {
            for (const [key, value] of Object.entries(category))
            {
                this.config.set(key, value as string);
            }
        }

        this.checkConfigVariables();
    }

    /**
     * Checks the map attribute with all confVars if one of those is undefined or empty.
     * @returns  {void}  returns Error if conf vars are undefined
     * @throws  Error Config not properly set
     */
    private static checkConfigVariables(): void | never
    {
        (this.config.get() as Map<string, string>).forEach(((value) =>
        {
            if (value === undefined || value === null || value === '')
            {
                throw new Error(`Config Variables haven't been set properly. You need to setup them in the config.json file.`);
            }
        }));
    }

}
