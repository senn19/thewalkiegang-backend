import {Loader} from '../../interfaces/Loader';
import * as express from 'express';
import {Express} from 'express';

export class ProjectLoader extends Loader
{
    public static projectsPath = `${__dirname}/projects/**/*.project.js`;
    public static events = {
        init: 'onInit',
        route: 'onRouting'
    };

    /**
     * Loads all components in projects and overwrites the fileList attribute
     */
    public static loadComponents(app: Express): void
    {
        // Get all necessary subPaths
        const subPaths = this.getSubPaths(),
              appMap = {};

        // Iterate through every project subPath to require component files and instantly load them
        for (const project in subPaths)
        {
            if (!subPaths.hasOwnProperty(project))
            {
                continue;
            }

            // Create subApp for mounting components
            appMap[subPaths[project]] = express();
            app.use(`/${subPaths[project]}`, appMap[subPaths[project]]);

            this.loadFiles(`${__dirname}/projects/${subPaths[project]}/components/**/*.com.js`);
            this.onEvent(this.events.init);
            this.onEvent(this.events.route, appMap[subPaths[project]]);
        }
    }
}
