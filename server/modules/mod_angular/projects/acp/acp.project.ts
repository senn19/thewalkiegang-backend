import {priority, Project} from '../../interfaces/Project';

export class AcpProject implements Project
{
    public priority = priority.core;

    public onInit(): void
    {
    }
}
