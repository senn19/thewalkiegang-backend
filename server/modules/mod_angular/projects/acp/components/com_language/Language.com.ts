import {Component} from '../../../../interfaces/Component';
import {Express} from 'express';
import {LanguageLoader} from './LanguageLoader';

export class LanguageCom implements Component
{
    private languages: any = {};

    public onInit(): void
    {
        // Load all language files in languages folder and store them in attribute
        LanguageLoader.loadJSON(LanguageLoader.languagePath);
        this.languages = LanguageLoader.getLang();
    }

    public onRouting(app: Express): void
    {
        // Loop through languages and create routes for them
        for (const lang of Object.keys(this.languages))
        {
            /**
             * @api {get} /language/${lang] Request Language Variables
             * @apiName GetLanguage
             * @apiGroup Language
             *
             * @apiParam {String} lang Language which should be requested.
             *
             * @apiSuccess {JSON} Language variables
             */
            app.get(`/language/${lang}`, (req, res) => {
                // Remove cached language files and reload them
                delete require.cache[require.resolve(this.languages[lang].path)];
                this.onInit();
                res.status(200).json(this.languages[lang].vars);
            });
        }
    }
}
