import {Loader} from '../../../../../../interfaces/Loader';

export class LanguageLoader extends Loader
{
    public static languagePath = `${__dirname}/languages/**/*.json`;

    public static getLang(): void
    {
        return this.fileList;
    }
}
