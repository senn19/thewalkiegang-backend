import {Module, priority} from '../../interfaces/Module';
import * as express from 'express';
import {ProjectLoader} from './ProjectLoader';

declare const __basePath, $cfg;

export class AngularMod implements Module
{
    public priority = priority.core;

    public onLoad(app): void
    {
        // Get all projects with their components in a list and invoke componentLoader
        console.warn('Loading existing Angular Projects');
        ProjectLoader.loadFiles(ProjectLoader.projectsPath);
        // Init projects for some additional functionality
        ProjectLoader.onEvent(ProjectLoader.events.init, app);
    }

    public onRouting(app): void
    {
        // Get all projects out of configuration file
        const projects: any = $cfg.get('_ANGULAR_PROJECTS');

        // Create for every compiled project dist a route
        for (const key of Object.keys(projects))
        {
            app.use(projects[key].URL_PATH, express.static(`${__basePath}/../client/dist/${projects[key].FOLDER}/`));
        }
    }

    public onServerStart(app): void
    {
        // Load all components of every project in priority order
        ProjectLoader.loadComponents(app);
    }

    public onFallback(app): void
    {
        app.get('/*', (req, res) => { res.redirect('/'); });
    }
}
