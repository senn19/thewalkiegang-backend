export interface Project
{
    // Define priority for priority queue loading
    priority: priority;

    // Load additional functionality before loading components
    onInit: () => void;
}

export enum priority {
    core,
    high,
    middle,
    low,
    default
}
