import {Express} from 'express';

export interface Component
{
    // Initialize component and its settings if necessary
    onInit: () => void;

    // Starts the routing of every component
    onRouting: (expressApp?: Express) => void;
}
