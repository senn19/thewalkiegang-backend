import {Express} from 'express';

export interface Module {

    // Define priority for priority queue loading
    priority?: priority;

    // Initialize core modules in priority queue
    onInit?: () => void;

    // Load additional functionality before routing
    onLoad?: (expressApp?: Express) => void;

    // Load different routes of different modules in priority queue
    onRouting?: (expressApp?: Express) => void;

    // Additional method to load anything after server has been started.
    onServerStart?: (expressApp?: Express) => void;

    // Fallback Routing which will be invoked in the end.
    onFallback?: (expressApp?: Express) => void;

}

export enum priority {
    core,
    high,
    middle,
    low,
    default
}
