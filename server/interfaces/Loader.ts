import {Express} from 'express';

export class Loader
{
    protected static fileList;

    /**
     * Loads a list of files from a given path
     * List attribute will stay empty if no files will be found
     */
    public static loadFiles(filePath: string): any
    {
        // Define glob, path and get all module files out of modules folder, ignores files starting with underscore
        const glob     = require('glob'),
              path     = require('path'),
              files    = glob.sync(filePath),
              fileList = {};

        // Filter files by given list and reassign into object
        for (const file of files)
        {
            const instance = require(path.resolve(file));

            // Continue loop if file is empty
            if (Object.keys(instance)[0] === undefined || Object.keys(instance)[0] === null)
            {
                continue;
            }

            // Assign current value to fileList with symLink to function
            fileList[Object.keys(instance)[0]] = {fn: new (Object.values(instance)[0] as any)(), path: file};
            console.info(`${Object.keys(instance)[0]} has been found...`);
        }

        this.fileList = fileList;
    }

    /**
     * Loads a list of json files from a given path
     * List attribute will stay empty if no files will be found
     */
    public static loadJSON(filePath: string)
    {
        // Define glob, path and get all module files out of modules folder, ignores files starting with underscore
        const glob     = require('glob'),
              path     = require('path'),
              files    = glob.sync(filePath),
              fileList = {};

        // Filter files by given list and reassign into object
        for (const file of files)
        {
            const json = require(path.resolve(file)),
                  fileName = path.normalize(file).split(path.sep)
                                 .pop().replace('.json', '');

            fileList[fileName] = { vars: json, path: file};
        }

        this.fileList = fileList;
    }

    /**
     * Invokes a given event in an already defined class. Specify events per Loader definition.
     * @param {string} event to trigger.
     * @param app Express instance, which could be provided
     */
    public static onEvent(event: string, app?: Express): void
    {
        // Sort classes by priority and get only the Function values.
        const fileClasses: any = Object.values(this.fileList).sort(this.sortByPriority);

        // Iterate through instances and trigger event methods.
        for (const instance of fileClasses)
        {
            if (typeof instance.fn[event] === 'function')
            {
                instance.fn[event](app);
            }
        }
    }

    /**
     * Method to sort an array on the basis of priority
     * @param  a  object of an array to compare
     * @param  b  object of an array to compare with
     * @return  -1 if smaller, 0 if equal, 1 if greater
     */
    protected static sortByPriority(a: any, b: any): number
    {
        if (a.fn.priority === null || a.fn.priority === undefined)
        {
            return 1;
        }

        if (b.fn.priority === null || b.fn.priority === undefined)
        {
            return -1;
        }

        if (a.fn.priority < b.fn.priority)
        {
            return -1;
        }

        if (a.fn.priority === b.fn.priority)
        {
            return 0;
        }

        return 1;
    }

    /**
     * Returns an array with subFolder names of loaded elements
     * @returns [name : path] array with name path pairing
     */
    protected static getSubPaths(): any
    {
        // Define path for normalizing and getting separator later on
        const path     = require('path'),
              subPaths = [];

        // Iterate through classes and load components
        for (const file of Object.entries(this.fileList) as any)
        {
            const pathArray: string[] = path.normalize(file[1].path).split(path.sep);
            subPaths[file[0]] = pathArray[pathArray.length - 2];
        }

        return subPaths;
    }
}
