
// Console logger overrides
const xtermColors  = {info: '\x1b[34m', warn: '\x1b[35m', error: '\x1b[31m', default: '\x1b[37m'},
      consoleTypes = ['info', 'warn', 'error'];

consoleTypes.forEach(
    (type) =>
    {
        const oldConsole = console[type].bind(console);
        console[type] = function()
        {
            oldConsole.apply(console, ['[' + new Date().toLocaleTimeString() + '] ' +
            xtermColors[type] + Array.prototype.join.call(arguments, ',') + xtermColors.default]);
        };
    });
