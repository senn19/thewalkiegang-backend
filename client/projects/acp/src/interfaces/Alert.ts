/* * * * * * * * *

 This file contains necessary subclasses of superclass alert.
 All important attributes will be stored in alert.

 * * * * * * * * */

export class Alert
{
    constructor(
        public type: string,
        public message: string,
        public timeOut?: any)
    {}

    /**
     * Checks if provided alert is equal to object that invoked this method
     * @param {Alert} alert to check on
     * @return {boolean} if objects are equal
     */
    public isEqual(alert: Alert): boolean
    {
        if (alert.message === this.message && alert.type === this.type)
        {
            return true;
        }
    }
}
