/* * * * * * * * *

 This file contains necessary subclasses of superclass permission.
 All important attributes will be stored in permission.

 * * * * * * * * */

export class Permission
{
    public id: string;

    constructor(public name: string, public description: string, public permission: PermissionString) {}
}

export enum PermissionString
{
    admin_access = 'admin_access',
    admin_full_access = 'admin_full_access',
    system_access = 'system_access',
    ticket_system_access = 'ticket_system_access',
    user_manager_access = 'user_manager_access',
    group_manager_access = 'group_manager_access'
}
