/* * * * * * * * *

 This file contains necessary subclasses of superclass config.
 All important attributes will be stored in config.

 * * * * * * * * */

export class Config<T>
{
    public id: string;
    public label: string;
    public placeholder: string;
    public inputType: CfgInputTypes;
    public hint?: string;

    public category: CfgCategories;
    public identifier: CfgIdentifiers;
    public value: T;
}

export enum CfgCategories
{
    general = 'general',
    maintenance = 'maintenance',
    metaData = 'metaData',
    ogData = 'ogData',
    cache = 'cache'
}

export enum CfgIdentifiers
{
    siteName = 'siteName',
    appName = 'appName',
    siteDesc = 'siteDesc',
    siteOffline = 'siteOffline',
    appOffline = 'appOffline',
    loginOffline = 'loginOffline',
    offlineMsg = 'offlineMsg',
    metaDesc = 'metaDesc',
    metaKeywords = 'metaKeywords',
    metaAuthor = 'metaAuthor',
    metaTitle = 'metaTitle',
    ogUrl = 'ogUrl',
    ogTitle = 'ogTitle',
    ogDesc = 'ogDesc',
    ogImage = 'ogImage',
    permCacheTime = 'permCacheTime',
    fallbackLanguage = 'fallbackLanguage',
}

export enum CfgInputTypes
{
    input = 'input',
    number = 'number',
    url = 'url',
    textarea = 'textarea',
    boolList = 'boolList',
}
