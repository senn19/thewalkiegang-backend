/* * * * * * * * *

 This file contains necessary subclasses of superclass user.
 All important attributes will be stored in user.

 * * * * * * * * */

import {Permission} from './Permission';

export class User
{
    public id: string;
    public username: string;
    public displayName: string;
    public disabled: boolean;

    constructor(public email?: string, public password?: string) {}
}

export class UserPerms extends User 
{
    public permissions: Permission[];
}
