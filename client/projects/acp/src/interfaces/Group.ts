/* * * * * * * * *

 This file contains necessary subclasses of superclass groups.
 All important attributes will be stored in groups.
 This classes will support permission management.

 * * * * * * * * */

import {Permission} from './Permission';

export class Group
{
    public id: string;
    public name: string;
    public description: string;
    public permissions: Permission[];
    public user: string[];

    constructor(){}
}
