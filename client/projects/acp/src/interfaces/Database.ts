/* * * * * * * * *

 This file contains necessary subclasses of superclass database.
 All important attributes will be stored in database.

 * * * * * * * * */

export enum Database
{
    user   = 'user',
    groups = 'groups',
    permissions = 'permissions',
    config = 'config'
}

export enum operator
{
    GT = '>',
    GE = '>=',
    LT = '<',
    LE = '<=',
    EQ = '==',
    AC = 'array-contains'
}

export class QueryStmt
{
    constructor(public key: string, public opt: operator, public value: string) {}
}
