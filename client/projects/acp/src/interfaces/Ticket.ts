/* * * * * * * * *

 This file contains necessary subclasses of superclass ticket.
 All important attributes will be stored in ticket.

 * * * * * * * * */
import {firestore} from 'firebase/app';
import Timestamp = firestore.Timestamp;

export class Ticket
{
    public id: string;
    public title: string;
    public author: string;

    public status: TicketStatus;
    public priority: TicketPriority;
    public category: TicketCategory;

    public createDate: Timestamp;
}

export class TicketMsg
{
    public id: string;
    public author: string;
    public type: TicketMsgType;
    public message: string;

    public createDate: Timestamp;
}

export enum TicketStatus
{
    open = 'PAGE_ACP_TICKET_SYSTEM_STATUS_OPEN',
    closed = 'PAGE_ACP_TICKET_SYSTEM_STATUS_CLOSED',
    solved = 'PAGE_ACP_TICKET_SYSTEM_STATUS_SOLVED'
}

export enum TicketPriority
{
    critical = 'PAGE_ACP_TICKET_SYSTEM_PRIORITY_CRITICAL',
    high = 'PAGE_ACP_TICKET_SYSTEM_PRIORITY_HIGH',
    medium = 'PAGE_ACP_TICKET_SYSTEM_PRIORITY_MEDIUM',
    low = 'PAGE_ACP_TICKET_SYSTEM_PRIORITY_LOW'
}

export enum TicketCategory
{
    app = 'PAGE_ACP_TICKET_SYSTEM_CATEGORY_APP',
    website = 'PAGE_ACP_TICKET_SYSTEM_CATEGORY_WEBSITE',
    technical = 'PAGE_ACP_TICKET_SYSTEM_CATEGORY_TECH_ISSUE',
    payment = 'PAGE_ACP_TICKET_SYSTEM_CATEGORY_PAYMENT',
    shop = 'PAGE_ACP_TICKET_SYSTEM_CATEGORY_SHOP'
}

export enum TicketMsgType
{
    msg,
    announcement
}
