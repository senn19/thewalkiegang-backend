export enum ValidityRuleset
{
    alphabetical
}

export interface SortProp
{
    property: string;
    value: boolean;
}

export class Utils
{

    /**
     * Creates a csv array string out of a set of data. Data keys will fill head of file.
     * @param {T[]} data - data which have to been converted to csv
     * @returns {string} - csv array output
     */
    public static createCSV<T>(data: T[]): string
    {
        const replacer: (key, value) => any = (key, value) => value === undefined || value === null || value === 'null' ? '' : value;
        const head: string[] = [];
        data.forEach((e) => Object.keys(e).forEach((k) => head.indexOf(k) < 0 ? head.push(k) : null));
        const csv = data.map((row) => head.map((field) => JSON.stringify(row[field], replacer)).join(','));
        csv.unshift(head.join(','));
        return csv.join('\r\n');
    }


    /**
     * Informally, the Levenshtein distance between two words is the minimum number of single-character edits
     * @param {string} a - string a
     * @param {string} b - string b
     * @returns {number} levenshtein distance
     */
    public static levenshtein(a: string, b: string): number
    {
        if (a.length === 0)
        {
            return b.length;
        }

        if (b.length === 0)
        {
            return a.length;
        }

        const matrix = [];

        for (let i = 0; i <= b.length; i++)
        {
            matrix[i] = [i];
        }

        for (let i = 0; i <= a.length; i++)
        {
            matrix[0][i] = [i];
        }

        for (let i = 1; i <= b.length; i++)
        {
            for (let k = 1; k <= a.length; k++)
            {
                if (b.charAt(i - 1) === a.charAt(k - 1))
                {
                    matrix[i][k] = matrix[i - 1][k - 1];
                }
                else
                {
                    matrix[i][k] = Math.min(matrix[i - 1][k - 1] + 1,
                        Math.min(matrix[i][k - 1] + 1,
                            matrix[i - 1][k] + 1));
                }
            }
        }

        return matrix[b.length][a.length];
    }

    /**
     * Validates provided string by checking provided ruleset.
     * @param {ValidityRuleset} ruleset
     * @param {string} value
     * @returns {boolean}
     */
    public static validateString(ruleset: ValidityRuleset, value: string): boolean
    {
        if (ruleset === ValidityRuleset.alphabetical)
        {
            return /^[A-Za-z_-äöüÄÖÜß ]+$/.test(value);
        }
    }

    /**
     * Filters an Iterable by levenshtein algorithm and returns an array with values equal to maxDistance
     * @param {Iterable<string>} iterator - iterable item like map.keys, entries ...
     * @param {string} filter - value to filter/compare with
     * @param {number} maxDistance - max levenshtein distance
     * @returns {string[]} array with values after filtering
     */
    public static filterByLevenshtein(iterator: Iterable<string>, filter: string, maxDistance: number): string[]
    {
        const filteredList = [];

        for (const item of iterator)
        {
            if (this.levenshtein(filter, item) <= maxDistance)
            {
                filteredList.push(item);
            }
        }

        return filteredList;
    }

    /**
     * Compares two generic properties on equality and returns 0 if equal, -1 if smaller or 1 if bigger
     * @param {T} a - first object with property
     * @param {T} b - second object with property
     * @param {string} property - property to checkOn
     * @param {boolean} reverseOrder - reverse natural sorting order
     * @param {boolean} count - count array entries if true and sorts by entries
     * @returns {number} 0 ===, -1 <, 1 >
     */
    public static compareProp<T>(a: T, b: T, property: string, reverseOrder: boolean = false, count: boolean = false): number
    {
        function nullCheck(callback: () => {}, emptyString: boolean = false): any
        {
            if (a[property] === undefined && b[property] === undefined || a[property] === null && b[property] === null)
            {
                return 0;
            }

            if (a[property] === undefined || a[property] === null)
            {
                return reverseOrder ? -1 : 1;
            }

            if (b[property] === undefined || b[property] === null)
            {
                return reverseOrder ? 1 : -1;
            }

            if (emptyString)
            {
                if (a[property] === '')
                {
                    return reverseOrder ? -1 : 1;
                }

                if (b[property] === '')
                {
                    return reverseOrder ? 1 : -1;
                }
            }

            return callback();
        }

        if (count && (a[property] instanceof Array || b[property] instanceof Array))
        {
            return nullCheck(() => a[property].length < b[property].length ? reverseOrder ? -1 : 1 :
                a[property].length > b[property].length ? reverseOrder ? 1 : -1 : 0);
        }

        if (typeof a[property] === 'number' || typeof b[property] === 'number')
        {
            return nullCheck(() => a[property] < b[property] ?
                reverseOrder ? -1 : 1 : a[property] > b[property] ? reverseOrder ? -1 : 1 : 0);
        }

        if (typeof a[property] === 'string' || typeof b[property] === 'string')
        {
            return nullCheck(() => reverseOrder ?
                (b[property] as string).localeCompare(a[property] as string) :
                (a[property] as string).localeCompare(b[property] as string), true);
        }

        return a[property] ? reverseOrder ? -1 : 1 : b[property] ? reverseOrder ? 1 : -1 : 0;
    }
}
