export class ImageLoader
{
    /**
     * @attribute assets: assets path
     */
    private readonly assets = 'assets/';

    constructor(private compPath: string) {
        this.assets += `${compPath}/`;
    }

    /**
     * Returns a valid image string to a given key (in case the image has been placed in that directory)
     * @param {string} key - image or icon
     * @returns {string} image path string
     */
    public get(key: string): string
    {
        return `${this.assets}${key}`;
    }

}
