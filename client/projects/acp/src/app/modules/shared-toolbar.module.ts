import {NgModule} from '@angular/core';
import {ListToolbarComponent} from '../includes/list-toolbar/list-toolbar.component';
import {CommonModule} from '@angular/common';
import {ListTableComponent} from '../includes/list-table/list-table.component';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FormToolbarComponent} from '../includes/form-toolbar/form-toolbar.component';
import {SimpleHeadingComponent} from '../includes/simple-heading/simple-heading.component';
import {BreadcrumbComponent} from '../includes/breadcrumb/breadcrumb.component';

@NgModule({
    declarations: [
        BreadcrumbComponent,
        ListToolbarComponent,
        ListTableComponent,
        FormToolbarComponent,
        SimpleHeadingComponent
    ],
    imports: [
        CommonModule,
        NgbPaginationModule,
        FormsModule,
        RouterModule
    ],
    exports     : [
        BreadcrumbComponent,
        ListToolbarComponent,
        ListTableComponent,
        FormToolbarComponent,
        SimpleHeadingComponent
    ]

})
export class SharedToolbarModule {}
