import {NgModule} from '@angular/core';
import {AlertBoxComponent} from '../includes/alert-box/alert-box.component';
import {LangSwitchComponent} from '../includes/lang-switch/lang-switch.component';
import {LoginBoxComponent} from '../includes/login-box/login-box.component';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports     : [
        CommonModule,
        NgbModule,
        FormsModule
    ],
    declarations: [
        AlertBoxComponent,
        LangSwitchComponent,
        LoginBoxComponent,
    ],
    exports     : [
        AlertBoxComponent,
        LangSwitchComponent,
        LoginBoxComponent,
        NgbModule,
        FormsModule,
    ]
})
export class SharedUiModule {}
