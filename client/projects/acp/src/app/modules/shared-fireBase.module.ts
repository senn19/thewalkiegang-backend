import {NgModule} from '@angular/core';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    imports: [
        AngularFireModule.initializeApp(environment.firebase, 'the-walkie-gang'),
        AngularFirestoreModule.enablePersistence(),
        AngularFireAuthModule,
        HttpClientModule,
    ],
    exports: [
        AngularFireModule,
        AngularFirestoreModule,
        AngularFireAuthModule,
        HttpClientModule
    ]
})
export class SharedFireBaseModule {}
