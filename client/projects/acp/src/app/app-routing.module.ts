import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {AdminGuard} from '../guards/core/admin.guard';
import {PermissionString} from '../interfaces/Permission';
import {AdminCpComponent} from './pages/admin-cp/admin-cp.component';
import {MetaGuard} from '../guards/core/meta.guard';
import {SiteNoticeComponent} from './pages/site-notice/site-notice.component';

const routes: Routes = [
    {
        path: '', canActivate: [MetaGuard], children: [
            { path: '', component: HomeComponent},
            { path: 'site-notice', component: SiteNoticeComponent},
            {
                path        : 'acp',
                canActivate : [AdminGuard],
                data        : {perm: [PermissionString.admin_access]},
                component   : AdminCpComponent,
                loadChildren: './pages/admin-cp/admin-cp.module#AdminCpModule'
            },
            {path: '**', redirectTo: '/'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
