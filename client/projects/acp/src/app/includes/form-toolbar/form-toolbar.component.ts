import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';

export interface FormToolbarEvent
{
    origin: FormToolbarEvents;
}

export declare interface OnToolbarEvent
{
    onToolbarEvent($event: FormToolbarEvent): void;
}

export enum FormToolbarEvents
{
    saveForm,
    cancelFarm,
    saveCloseForm
}

@Component({
    selector   : 'app-form-toolbar',
    templateUrl: './form-toolbar.component.html',
    styleUrls  : ['./form-toolbar.component.scss']
})
export class FormToolbarComponent
{
    @Input() languageString: string;
    @Output() formToolbarEvent: EventEmitter<FormToolbarEvent> = new EventEmitter<FormToolbarEvent>();

    constructor(public lang: LanguageService) { }


    // Triggers a FormToolbar save-event
    saveForm(): void
    {
        this.formToolbarEvent.emit({origin: FormToolbarEvents.saveForm});
    }

    // Triggers a FormToolbar saveClose-event
    saveAndCloseForm(): void
    {
        this.formToolbarEvent.emit({origin: FormToolbarEvents.saveCloseForm});
    }

    // Triggers a FormToolbar cancel-event
    cancelForm(): void
    {
        this.formToolbarEvent.emit({origin: FormToolbarEvents.cancelFarm});
    }
}
