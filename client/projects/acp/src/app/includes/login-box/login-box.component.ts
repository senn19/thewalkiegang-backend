import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/AuthManager/authentication.service';
import {AlertService} from '../../../services/AlertManager/alert.service';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {User} from '../../../interfaces/User';
import {Subscription} from 'rxjs';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {PermissionService} from '../../../services/PermManager/permission.service';
import {ImageLoader} from '../../../utils/ImageLoader';
import {Router} from '@angular/router';

interface ButtonStyle
{
    type: LoginBoxStyle;
    classes?: string;
}

export enum LoginBoxStyle
{
    modalText,
    modalIcon,
    logoutIcon,
    loginIcon,
}

@Component({
    selector   : 'app-login-box',
    templateUrl: './login-box.component.html',
    styleUrls  : ['./login-box.component.scss'],
})

export class LoginBoxComponent implements OnInit, OnDestroy
{
    LoginBoxStyle = LoginBoxStyle;

    // User object for login form
    user: User = new User();
    // Current user object
    currUser: User;
    currUserSub: Subscription;
    // Active modal window reference
    activeModal: NgbModalRef;

    icons: ImageLoader = new ImageLoader('icons');

    /**
     * Define style of loginBox component
     * @type {string} 'text-button' |
     */
    @Input() buttonStyle: ButtonStyle = {type: LoginBoxStyle.modalText};
    @Input() modalAnim = 'scaleIn';
    @Input() redirectHome: boolean;

    constructor(private auth: AuthenticationService, private alerts: AlertService, public lang: LanguageService,
                private modal: NgbModal, private perm: PermissionService, private router: Router)
    {
    }

    ngOnInit()
    {
        if (this.auth.user)
        {
            this.currUserSub = this.auth.user.subscribe((user) =>
            {
                this.currUser = user;
            });
        }
    }

    ngOnDestroy(): void
    {
        if (this.currUserSub)
        {
            this.currUserSub.unsubscribe();
        }
    }

    // Invokes login with google provider
    loginWithGoogle(): void
    {
        const btn: HTMLElement = document.querySelector('.btn-login-with-google'),
              currHTML: string = btn.innerHTML;
        btn.innerHTML = '<span class="spinner-border spinner-border-sm text-dark"></span>';

        this.auth.loginWithGoogle()
            .then(() =>
            {
                if (this.activeModal)
                {
                    this.activeModal.close();
                }
                btn.innerHTML = currHTML;
                this.alerts.success(this.lang.get('SUCCESS_AUTH_SIGN_IN'), 5);
            })
            .catch(error =>
            {
                btn.innerHTML = currHTML;
                this.alerts.error(this.lang.get(error.message), 7);
            });
    }

    // Invokes login with provided user object.
    loginWithEmailAndPassword(): void
    {
        const btn: HTMLElement = document.querySelector('.btn-login-with-email-and-password'),
              currHTML: string = btn.innerHTML;
        btn.innerHTML = '<span class="spinner-border spinner-border-sm text-light"></span>';

        this.auth.loginWithEmailAndPassword(this.user)
            .then(() =>
            {
                if (this.activeModal)
                {
                    this.activeModal.close();
                }
                btn.innerHTML = currHTML;
                this.alerts.success(this.lang.get('SUCCESS_AUTH_SIGN_IN'), 5);
            })
            .catch(error =>
            {
                btn.innerHTML = currHTML;
                this.alerts.error(this.lang.get(error.message), 7);
            });
    }

    // Invokes logout
    logout(): void
    {
        if (this.redirectHome)
        {
            this.router.navigate(['/home']);
        }

        this.perm.clearCachedPerms();
        this.auth.logout()
            .then(() => this.alerts.success(this.lang.get('SUCCESS_AUTH_SIGN_OUT'), 5));
    }

    // Redirects to current user profile edit
    redirectProfile(): void
    {
        this.router.navigate(['/acp/user/profiles/form/', this.currUser.id]);
    }

    // Open modal window and store reference
    open(loginBox): void
    {
        document.querySelector('.alert-modal').classList.add('hidden');
        this.activeModal = this.modal.open(loginBox, {
            windowClass   : `${this.modalAnim}`, centered: true,
            ariaLabelledBy: this.lang.get('PAGE_LOGIN_TITLE')
        });
        this.activeModal.result.finally(() => document.querySelector('.alert-modal').classList.remove('hidden'));
    }
}
