import {Component, Input, OnInit} from '@angular/core';
import {AlertService} from '../../../services/AlertManager/alert.service';

@Component({
    selector   : 'app-alert-box',
    templateUrl: './alert-box.component.html',
    styleUrls  : ['./alert-box.component.scss']
})
export class AlertBoxComponent implements OnInit
{
    /**
     * @attribute simple: will stop handling alerts via array if set to true
     * @attribute style: (box) displaying as natural element, (modal) displaying as modal popUp
     */
    @Input() simple = false;
    @Input() style = 'box';

    constructor(public alerts: AlertService) {
    }

    ngOnInit()
    {
        this.alerts.simple = this.simple;
    }

}
