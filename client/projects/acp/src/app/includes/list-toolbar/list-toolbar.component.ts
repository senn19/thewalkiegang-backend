import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';

export interface ListToolbarEvent
{
    origin: ListToolbarEvents;
    value?: any;
}

export declare interface OnToolbarEvent
{
    onToolbarEvent($event: ListToolbarEvent): void;
}

export enum ListToolbarEvents
{
    createBtn,
    deleteBtn,
    customHref
}

export interface ToolbarCustomHref
{
    text: string;
    href: string;
    class: string;
}

@Component({
    selector   : 'app-list-toolbar',
    templateUrl: './list-toolbar.component.html',
    styleUrls  : ['./list-toolbar.component.scss']
})
export class ListToolbarComponent<T>
{
    @Input() languageString: string;
    @Input() list: T[];
    @Input() placeholder = 'MODULE_FILTER_PLACEHOLDER_SEARCH';
    @Output() filteredList = new EventEmitter<T[]>();
    @Output() pageSize = new EventEmitter<number>();

    @Output() listToolbarEvent: EventEmitter<ListToolbarEvent> = new EventEmitter<ListToolbarEvent>();

    @Input() allowCreate;
    @Input() allowDeleteMode;

    @Input() allowCustomHref: ToolbarCustomHref[];

    constructor(public lang: LanguageService) {}

    createEvent(): void
    {
        this.listToolbarEvent.emit({origin: ListToolbarEvents.createBtn});
    }

    deleteEvent(): void
    {
        this.listToolbarEvent.emit({origin: ListToolbarEvents.deleteBtn});
    }

    customEvent(index: number): void
    {
        this.listToolbarEvent.emit({origin: ListToolbarEvents.customHref, value: index});
    }

    // Updates pageSize on selectChange
    updatePageSize($event): void
    {
        this.pageSize.emit($event.target.value);
    }

    /**
     * Filters current imported list by provided keyboard event.
     * First of all it looks through translated values,
     * afterwards it checks language strings.
     * @param {KeyboardEvent} $event - current keyBoard keyup
     */
    filterList($event: KeyboardEvent): void
    {
        if (($event.target as any).value === '')
        {
            return this.filteredList.emit(this.list);
        }

        const filtered = this.list.filter(
            (el: T): boolean =>
            {
                for (const key of Object.keys(el))
                {
                    if (key === 'id' || !el[key])
                    {
                        continue;
                    }

                    if (typeof el[key] === 'string' || typeof el[key] === 'number')
                    {
                        if (this.lang.get(el[key]) && this.lang.get(el[key]).indexOf(($event.target as any).value) >= 0)
                        {
                            return true;
                        }

                        if (el[key].indexOf(($event.target as any).value) >= 0)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        );

        this.filteredList.emit(filtered);
    }

    // Redirects to custom href if provided
    redirectTo(url: string): void
    {
        if (url)
        {
            window.open(url, 'blank');
        }
    }
}
