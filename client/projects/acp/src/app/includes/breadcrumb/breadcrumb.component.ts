import {Component, OnInit} from '@angular/core';
import {BreadcrumbMap} from './breadcrumbMap';
import {Router} from '@angular/router';
import {LanguageService} from '../../../services/LanguageManager/language.service';

@Component({
    selector   : 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls  : ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit
{
    
    breadcrumbMap: BreadcrumbMap[] = BreadcrumbMap;
    displayBreadcrumb: BreadcrumbMap;

    constructor(private router: Router, public lang: LanguageService) { }

    ngOnInit()
    {
        this.displayBreadcrumb = this.breadcrumbMap.find(bc => bc.routePath === this.router.url ||
        bc.routePath.charAt(bc.routePath.length - 1) === '*' && this.router.url.startsWith(bc.routePath.slice(0, -1)));
    }

}
