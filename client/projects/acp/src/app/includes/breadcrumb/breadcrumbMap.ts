export interface BreadcrumbMap
{
    title: string;
    routePath: string;
    subPath?: BreadcrumbMap[];
}

export const BreadcrumbMap: BreadcrumbMap[] = [
    {
        title  : 'BREADCRUMB_SYSTEM', routePath: '/acp/system',
        subPath: [{title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'}]
    },
    {
        title  : 'BREADCRUMB_USER_MANAGER', routePath: '/acp/user/profiles',
        subPath: [{title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'}]
    },
    {
        title  : 'BREADCRUMB_USER_MANAGER_EDIT', routePath: '/acp/user/profiles/form/*',
        subPath: [
            {title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'},
            {title: 'BREADCRUMB_USER_MANAGER', routePath: '/acp/user/profiles'}
        ]
    },
    {
        title  : 'BREADCRUMB_GROUP_MANAGER', routePath: '/acp/user/groups',
        subPath: [{title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'}]
    },
    {
        title  : 'BREADCRUMB_GROUP_MANAGER_EDIT', routePath: '/acp/user/groups/form/*',
        subPath: [
            {title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'},
            {title: 'BREADCRUMB_USER_MANAGER', routePath: '/acp/user/groups'}
        ]
    },
    {
        title  : 'BREADCRUMB_USER_MANAGER_CREATE', routePath: '/acp/user/groups/form',
        subPath: [
            {title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'},
            {title: 'BREADCRUMB_USER_MANAGER', routePath: '/acp/user/groups'}
        ]
    },
    {
        title  : 'BREADCRUMB_PERMISSIONS', routePath: '/acp/user/perms',
        subPath: [{title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'}]
    },
    {
        title  : 'BREADCRUMB_TICKET_SYSTEM', routePath: '/acp/ticket-system',
        subPath: [{title: 'BREADCRUMB_DASHBOARD', routePath: '/acp'}]
    }
];
