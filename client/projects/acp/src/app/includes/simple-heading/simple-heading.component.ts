import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-simple-heading',
  templateUrl: './simple-heading.component.html',
  styleUrls: ['./simple-heading.component.scss']
})
export class SimpleHeadingComponent implements OnInit {

  @Input() languageString: string;

  constructor() { }

  ngOnInit() {
  }

}
