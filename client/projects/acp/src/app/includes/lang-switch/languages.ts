export interface Language {
    lang: string;
    key: string;
}

export const languages = [
    {lang: 'de', key: 'PAGE_HOME_SWITCH_LANG_DE'},
    {lang: 'en', key: 'PAGE_HOME_SWITCH_LANG_EN'},
    {lang: 'ru', key: 'PAGE_HOME_SWITCH_LANG_RU'},
];
