import {Component, Input} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {ImageLoader} from '../../../utils/ImageLoader';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Language, languages} from './languages';

interface ButtonStyle
{
    type: LangSwitchStyle;
    classes?: string;
}

export enum LangSwitchStyle {
    dropdown,
    modal
}

@Component({
    selector   : 'app-lang-switch',
    templateUrl: './lang-switch.component.html',
    styleUrls  : ['./lang-switch.component.scss']
})

export class LangSwitchComponent
{
    LangSwitchStyle = LangSwitchStyle;
    // Active modal window reference
    activeModal: NgbModalRef;

    @Input() buttonStyle: ButtonStyle = {type: LangSwitchStyle.dropdown};

    icons: ImageLoader = new ImageLoader('icons');
    languages: Language[] = languages;

    constructor(public lang: LanguageService, private modal: NgbModal) { }

    open(languageBox): void
    {
        this.activeModal = this.modal.open(languageBox, {
            windowClass   : 'scaleIn', centered: true,
            ariaLabelledBy: this.lang.get('PAGE_HOME_SWITCH_LANG')
        });
        this.activeModal.result.finally(() => document.querySelector('.alert-modal').classList.remove('hidden'));
    }
}
