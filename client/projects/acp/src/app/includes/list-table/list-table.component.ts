import {Component, EventEmitter, Input, Output} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {ImageLoader} from '../../../utils/ImageLoader';

export interface TableMap
{
    /**
     * @attribute head: TableHead
     * @attribute property: Object property for TableBody f.e. user[property]
     * @attribute lang: Translate this value if true
     * @attribute nullValue: default value if null or undefined
     * @attribute count: writes the length of an array instead of its value
     * @attribute coloredPin and invertedPin display a green and a red pin. InvertedPin inverts the condition.
     */
    head: string;
    property: string;
    lang?: boolean;
    nullValue?: string;
    count?: boolean;
    coloredPin?: boolean;
    invertedPin?: boolean;
    pos?: string;
}

export interface TableEvent<T>
{
    /**
     * @attribute event: current event, that has been triggered
     * @attribute value: value of this event
     */
    origin: TableEvents;
    value: T;
}

export declare interface OnTableEvent<T>
{
    onTableEvent($event: TableEvent<T>): void;
}

export enum TableEvents
{
    pinClick,
    trClick,
    headClick
}

@Component({
    selector   : 'app-list-table',
    templateUrl: './list-table.component.html',
    styleUrls  : ['./list-table.component.scss']
})
export class ListTableComponent<T>
{
    /**
     * @attribute tableMap: Setup for table, contains information about heading and property
     * @attribute list: generic table elements
     * @attribute editAble: just turns 'event' handling on or off (as well as cursor pointer)
     * @attribute page: start table at page xyz
     * @attribute pageSize: how many entries should be display on initialization
     * @attribute showIDs: shows index of entries in list
     */
    @Input() tableMap: TableMap[];
    @Input() list: T[];
    @Input() editAble: boolean;

    @Input() page = 1;
    @Input() pageSize = 25;

    @Input() showIDs = true;

    @Output() tableEvent: EventEmitter<TableEvent<T>> = new EventEmitter<TableEvent<T>>();

    icons: ImageLoader = new ImageLoader('icons');

    constructor(public lang: LanguageService) {}

    // Triggers on pin-click a tableEvent with an origin and a value
    triggerPinEvent(item: T): void
    {
        this.tableEvent.emit({origin: TableEvents.pinClick, value: item});
    }

    // Triggers on tr-click a tableEvent with an origin and a value
    triggerRowEvent(item: T): void
    {
        if (this.editAble)
        {
            this.tableEvent.emit({origin: TableEvents.trClick, value: item});
        }
    }

    // Triggers on th-click a tableEvent with an origin and a value
    triggerHeadEvent(property: T): void
    {
        this.tableEvent.emit({origin: TableEvents.headClick, value: property});
    }
}
