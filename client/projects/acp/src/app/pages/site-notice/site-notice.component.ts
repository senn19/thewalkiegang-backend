import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';

@Component({
  selector: 'app-site-notice',
  templateUrl: './site-notice.component.html',
  styleUrls: ['./site-notice.component.scss']
})
export class SiteNoticeComponent implements OnInit {

  constructor(public lang: LanguageService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
