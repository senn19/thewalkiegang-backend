import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AdminSystemComponent} from './admin-system.component';
import {AdminGuard} from '../../../guards/core/admin.guard';
import {PermissionString} from '../../../interfaces/Permission';

export const routes: Routes = [
    {
        path       : '',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.system_access, PermissionString.admin_full_access], acp: true},
        component  : AdminSystemComponent
    }
];

export const AdminSystemRouting: ModuleWithProviders = RouterModule.forChild(routes);
