import {Component, OnDestroy, OnInit} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {ConfigService} from '../../../services/ConfigManager/config.service';
import {CfgCategories, CfgInputTypes, Config} from '../../../interfaces/Config';
import {Subscription} from 'rxjs';
import {FormToolbarEvent, FormToolbarEvents, OnToolbarEvent} from '../../includes/form-toolbar/form-toolbar.component';
import {Router} from '@angular/router';
import {AlertService} from '../../../services/AlertManager/alert.service';
import {Language, languages} from '../../includes/lang-switch/languages';

@Component({
    selector   : 'app-admin-system',
    templateUrl: './admin-system.component.html',
    styleUrls  : ['./admin-system.component.scss']
})
export class AdminSystemComponent implements OnInit, OnDestroy, OnToolbarEvent
{
    // Store inputTypes and Categories enums for mvc bindings
    inputTypes = CfgInputTypes;
    categories = CfgCategories;

    // List of all languages
    languages: Language[] = languages;

    // Storage for configuration collection and updated configurations
    config: Config<any>[] = [];
    updatedElements: Config<any>[] = [];
    // Subscription reference
    configSub: Subscription;

    // Pop up on 'unsavedChanges', redirection on 'andClose'
    unsavedChanges: boolean;
    andClose: boolean;

    constructor(public lang: LanguageService, private cfg: ConfigService, private router: Router,
                private alert: AlertService) {}

    // Subscribe to configuration collection onInit
    ngOnInit(): void
    {
        this.configSub = this.cfg.getConfiguration().subscribe((config: Config<any>[]) =>
        {
            config.sort((a, b) => a.inputType > b.inputType ? 1 : a.inputType < b.inputType ? -1 : 0);
            this.config = config;
        });
    }

    // Unsubscribe onDestroy
    ngOnDestroy(): void
    {
        if (this.configSub)
        {
            this.configSub.unsubscribe();
        }
    }

    // Catch all ToolbarEvents and decide through switch case how UI Events should be handled
    onToolbarEvent($event: FormToolbarEvent): void
    {
        switch ($event.origin)
        {
            case FormToolbarEvents.cancelFarm:
                this.router.navigate(['/acp']);
                break;
            case FormToolbarEvents.saveForm:
                return this.save();
            case FormToolbarEvents.saveCloseForm:
                this.andClose = true;
                return this.save();
        }
    }

    // Validates and saves items in database
    save(): void
    {
        if (this.updatedElements.length === 0)
        {
            return this.alert.error(this.lang.get('ERROR_SYSTEM_NO_CHANGES_HAVE_BEEN_MADE'), 7);
        }

        this.cfg.updateConfiguration(this.updatedElements)
            .then(() =>
            {
                if (this.andClose)
                {
                    this.router.navigate(['/acp']);
                }

                this.unsavedChanges = false;
                this.updatedElements = [];
                this.alert.success(this.lang.get('SUCCESS_SYSTEM_UPDATE_CONFIGURATION'), 7);
            })
            .catch((error) => this.alert.error(error.message, 7));
    }

    // Reacts on changed entries and adds them to updatedArray // updates them there.
    onChangedEntry(config: Config<string | number>): void
    {
        if (config.value === undefined || config.value === null)
        {
            return;
        }

        this.addToUpdatedConfig<string | number>(config);
    }

    // Reacts on ListElement clicks, inverts boolean and invokes add/update method
    onListClick(config: Config<boolean>): void
    {
        config.value = !config.value;
        this.addToUpdatedConfig<boolean>(config);
    }

    // Method for assigning updatedElements array with updatedElements
    addToUpdatedConfig<T>(config: Config<T>)
    {
        // If already pushed into updated documents, get this one
        const updatedConfig: Config<T> = this.updatedElements.find(
            c => c.identifier === config.identifier);

        this.unsavedChanges = true;

        // Check if it already exists, update or add it therefore
        if (updatedConfig)
        {
            this.updatedElements[this.updatedElements.indexOf(updatedConfig)] = config;
        }
        else
        {
            this.updatedElements.push(config);
        }
    }


}

