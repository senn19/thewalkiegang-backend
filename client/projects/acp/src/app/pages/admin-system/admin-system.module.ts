import {NgModule} from '@angular/core';
import {AdminSystemComponent} from './admin-system.component';
import {CommonModule} from '@angular/common';
import {AdminSystemRouting} from './admin-system.routing';
import {NgbAlertModule, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedToolbarModule} from '../../modules/shared-toolbar.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        AdminSystemComponent
    ],
    imports: [
        CommonModule,
        AdminSystemRouting,
        NgbTabsetModule,
        SharedToolbarModule,
        FormsModule,
        NgbAlertModule
    ]
})
export class AdminSystemModule {}
