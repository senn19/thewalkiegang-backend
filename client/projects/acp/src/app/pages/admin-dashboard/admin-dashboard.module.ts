import {NgModule} from '@angular/core';
import {AdminDashboardComponent} from './admin-dashboard.component';
import {CommonModule} from '@angular/common';
import {AdminDashboardRouting} from './admin-dashboard.routing';
import {SharedToolbarModule} from '../../modules/shared-toolbar.module';
import {DashBoxComponent} from './dash-box/dash-box.component';

@NgModule({
    declarations: [
        AdminDashboardComponent,
        DashBoxComponent,
    ],
    imports: [
        CommonModule,
        AdminDashboardRouting,
        SharedToolbarModule
    ]
})
export class AdminDashboardModule {}
