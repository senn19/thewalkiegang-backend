import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-dash-box',
  templateUrl: './dash-box.component.html',
  styleUrls: ['./dash-box.component.scss']
})
export class DashBoxComponent implements OnInit {

  /**
   * @attribute text: current text of dash-box, can be a string as well as a number
   * @attribute icon: icon which will be displayed in the background of the box
   * @attribute languageString: description of dash-box
   * @attribute routePath: reference to page, which dash-box will link onto.
   * @attribute boxClass: class reference, scss box coloring f.e.
   */
  @Input() text: number|string;
  @Input() icon: string;
  @Input() languageString: string;
  @Input() routePath: string;
  @Input() boxClass: string;

  constructor() { }

  ngOnInit() {
  }

}
