import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AdminDashboardComponent} from './admin-dashboard.component';

export const routes: Routes = [{path: '', component: AdminDashboardComponent}
];

export const AdminDashboardRouting: ModuleWithProviders = RouterModule.forChild(routes);
