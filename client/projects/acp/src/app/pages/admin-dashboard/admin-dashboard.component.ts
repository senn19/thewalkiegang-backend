import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {DatabaseService} from '../../../services/DatabaseController/database.service';
import {Subscription} from 'rxjs';
import {User} from '../../../interfaces/User';
import {Database} from '../../../interfaces/Database';
import {Group} from '../../../interfaces/Group';
import {CfgIdentifiers, Config} from '../../../interfaces/Config';
import {ConfigService} from '../../../services/ConfigManager/config.service';

@Component({
    selector   : 'app-admin-dashboard',
    templateUrl: './admin-dashboard.component.html',
    styleUrls  : ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit
{

    // User countable and subscription
    userAmount = 0;
    userSub: Subscription;

    // Group countable and subscription
    groupsAmount = 0;
    groupsSub: Subscription;

    // Current app maintenance status as well as configuration subscription
    appOffline: Config<boolean>;
    confSub: Subscription;

    // Amount of open tickets
    openTickets = 0;

    constructor(public lang: LanguageService, private db: DatabaseService, private cfg: ConfigService) { }

    ngOnInit()
    {
        // Stores current amount of user which have not been disabled
        this.userSub = this.db.observeItems<User>(Database.user)
            .subscribe((user: User[]) => this.userAmount = user.filter(u => !u.disabled).length);

        // Stores current amount of groups
        this.groupsSub = this.db.observeItems<Group>(Database.groups)
            .subscribe((group: Group[]) => this.groupsAmount = group.length);

        // Stores current configuration file for 'appOffline' identifier
        this.confSub = this.cfg.getConfiguration().subscribe((cfg: Config<boolean>[]) =>
            this.appOffline = cfg.find(c => c.identifier === CfgIdentifiers.appOffline));
    }

    // Destroy current subscriptions
    ngOnDestroy()
    {
        if (this.userSub)
        {
            this.userSub.unsubscribe();
        }

        if (this.groupsSub)
        {
            this.groupsSub.unsubscribe();
        }

        if (this.confSub)
        {
            this.confSub.unsubscribe();
        }
    }

}
