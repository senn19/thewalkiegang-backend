import {PermissionString} from '../../../interfaces/Permission';

export interface Menu
{
    icon: string;
    menu: string;
    path: string;
    href?: string;
    permissionStrings?: PermissionString[];
    subMenu?: Menu[];
}

export const AcpMenu: Menu[] = [
    {icon: 'home.svg', menu: 'PAGE_ACP_MENU_DASHBOARD', path: './'},
    {
        icon             : 'config.svg',
        menu             : 'PAGE_ACP_MENU_SYSTEM',
        path             : 'system',
        permissionStrings: [PermissionString.admin_full_access, PermissionString.system_access]
    },
    {
        icon             : 'user.svg',
        menu             : 'PAGE_ACP_MENU_USER',
        path             : 'user',
        permissionStrings: [
            PermissionString.admin_full_access,
            PermissionString.user_manager_access,
            PermissionString.group_manager_access
        ],
        subMenu          : [
            {
                icon             : 'user.svg',
                menu             : 'PAGE_ACP_MENU_USER_MANAGER',
                path             : 'profiles',
                permissionStrings: [PermissionString.admin_full_access, PermissionString.user_manager_access]
            },
            {
                icon             : 'group.svg',
                menu             : 'PAGE_ACP_MENU_USER_GROUP_MANAGER',
                path             : 'groups',
                permissionStrings: [PermissionString.admin_full_access, PermissionString.group_manager_access]
            },
            {icon                : 'keylock.svg',
                menu             : 'PAGE_ACP_MENU_USER_PERMS_MANAGER',
                path             : 'perms',
                permissionStrings: [PermissionString.admin_full_access, PermissionString.group_manager_access]
            },
        ]
    },
    {
        icon             : 'ticket.svg',
        menu             : 'PAGE_ACP_MENU_TICKET_SYSTEM',
        permissionStrings: [PermissionString.admin_full_access, PermissionString.ticket_system_access],
        path             : 'ticket-system'
    },
    {
        icon : 'firebase.png',
        menu : 'PAGE_ACP_MENU_FIREBASE',
        permissionStrings: [PermissionString.admin_full_access],
        path : './',
        href : 'https://console.firebase.google.com/u/0/project/thewalkiegang/'
    }
];
