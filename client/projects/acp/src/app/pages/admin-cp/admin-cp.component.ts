import {AfterContentChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {Router} from '@angular/router';
import {PermissionService} from '../../../services/PermManager/permission.service';
import {UserPerms} from '../../../interfaces/User';
import {Subscription} from 'rxjs';
import {PermissionString} from '../../../interfaces/Permission';
import {AcpMenu, Menu} from './admin-menu';
import {ImageLoader} from '../../../utils/ImageLoader';
import {LoginBoxStyle} from '../../includes/login-box/login-box.component';
import {LangSwitchStyle} from '../../includes/lang-switch/lang-switch.component';
import {AuthenticationService} from '../../../services/AuthManager/authentication.service';
import {AlertService} from '../../../services/AlertManager/alert.service';

@Component({
    selector   : 'app-admin-cp',
    templateUrl: './admin-cp.component.html',
    styleUrls  : ['./admin-cp.component.scss']
})
export class AdminCpComponent implements OnInit, AfterViewInit, AfterContentChecked
{
    // Current user object and subscription reference
    currUser: UserPerms = new UserPerms();
    currUserSub: Subscription;

    // Current path user is on
    currPath: string = this.router.url;

    // MenuItems which have to been listed through view-template
    menuItems: Menu[] = AcpMenu;

    // Just an enum for loginBoxStyles
    LoginBoxStyle = LoginBoxStyle;
    LangSwitchStyle = LangSwitchStyle;

    icons: ImageLoader = new ImageLoader('icons');

    constructor(public lang: LanguageService, private router: Router, private perm: PermissionService,
                private auth: AuthenticationService, private alerts: AlertService) {}

    // OnInit will open sideBar and subscribe to current logged in user with permissions
    ngOnInit()
    {
        this.currUserSub = this.perm.getPermissions().subscribe((user) =>
        {
            this.currUser = user;
        });
    }

    // After view has initialized current active tab will be rendered as 'active' in sidebar
    ngAfterViewInit(): void
    {
        this.setCurrentPageToActive();
    }

    // Changes sideBar active after outlet has switched
    ngAfterContentChecked(): void
    {
        if (this.router.url !== this.currPath)
        {
            this.currPath = this.router.url;
            this.toggleActive(document.querySelector('ul.side-navbar-nav li.active'));
            this.setCurrentPageToActive();
        }
    }

    // Method for toggling sideBar, starts at false/null by default
    toggleSideBar(): void
    {
        const wrapper: HTMLElement    = document.querySelector<HTMLElement>('.content-wrapper'),
              sideBar: HTMLElement    = document.querySelector<HTMLElement>('.side-navbar'),
              burgerMenu: HTMLElement = document.querySelector<HTMLElement>('#burger-icon');

        burgerMenu.classList.toggle('open');
        wrapper.classList.toggle('toggled-wrapper');
        sideBar.classList.toggle('toggled-sideBar');

        const menuNode: NodeList = document.querySelectorAll<HTMLAnchorElement>('.side-navbar a.nav-link.d-none');

        // Hide all anchor elements
        if (menuNode.length > 0)
        {
            setTimeout(() => document.querySelectorAll<HTMLAnchorElement>('.side-navbar a.nav-link')
                .forEach(e => e.classList.toggle('d-none')), 340);
        }
        else
        {
            document.querySelectorAll<HTMLAnchorElement>('.side-navbar a.nav-link')
                .forEach(e => e.classList.toggle('d-none'));
        }
    }

    // Method for DropDown toggling of subMenus, accepts $event.currentTargets with 'data-target="#dropdown-${numberOfMenu}'
    toggleDropDown($event: MouseEvent): void
    {
        const ddId: string = ($event.currentTarget as HTMLElement).dataset.target.split('-')[1];
        document.querySelector<HTMLElement>(`#dropdown-${ddId}`).classList.toggle('toggled-down');
    }

    // Toggles MenuTab to active
    toggleActive($item: HTMLElement): void
    {
        $item.classList.toggle('active');
    }

    // Iterates through menu as well as subMenus and sets active equal to current urlPath
    setCurrentPageToActive(): void
    {
        // Split url into pieces and shift first element containing 'acp'
        const tempPath: string[] = this.currPath.substr(1).split('/');
        tempPath.shift();

        // Iterate through menu items, if it has a subMenu it is only necessary to get the menuItem out of it.
        for (const navItem of this.menuItems)
        {
            if (tempPath.length === 0 || tempPath.find(p => p === navItem.path))
            {
                const indexOf: number = this.menuItems.indexOf(navItem);

                if (navItem.subMenu && navItem.subMenu.length !== 0)
                {
                    // Toggle down the subMenu and find toggle item if it exists
                    document.querySelector<HTMLElement>(`#dropdown-${indexOf}`).classList.add('toggled-down');
                    const subItem = navItem.subMenu.find(item => tempPath.find(p => p === item.path));
                    const subOf: number = subItem ? navItem.subMenu.indexOf(subItem) : -1;

                    return subOf >= 0 ? this.toggleActive(document.querySelector(`li[data-target="item-${indexOf}-sub-${subOf}"]`)) : null;
                }

                document.querySelectorAll<HTMLElement>('.toggled-down').forEach(e => e.classList.remove('toggled-down'));
                return this.toggleActive(document.querySelector(`li[data-target="item-${indexOf}"]`));
            }
        }
    }

    // Checks permission of user for itemMenu
    check(permissionStrings: PermissionString[]): boolean
    {
        if (!this.currUser)
        {
            return false;
        }

        if (!permissionStrings)
        {
            return true;
        }

        // Permission check call
        return this.perm.checkOnPermissions(this.currUser.permissions, permissionStrings);
    }

    // Method handling for external urls
    redirectTo(url: string): void
    {
        if (url)
        {
            window.open(url, 'blank');
        }
    }

    sideLogout(): void
    {
        this.router.navigate(['/home']);

        this.perm.clearCachedPerms();
        this.auth.logout()
            .then(() => this.alerts.success(this.lang.get('SUCCESS_AUTH_SIGN_OUT'), 5));
    }
}
