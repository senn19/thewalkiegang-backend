import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminCpComponent} from './admin-cp.component';
import {AdminRouting} from './admin-cp.routing';
import {SharedUiModule} from '../../modules/shared-ui.module';

@NgModule({
    declarations: [
        AdminCpComponent,
    ],
    imports     : [
        CommonModule,
        SharedUiModule,
        AdminRouting,
    ],
    exports     : [
        AdminCpComponent
    ]
})
export class AdminCpModule {}
