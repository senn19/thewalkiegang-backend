import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AdminGuard} from '../../../guards/core/admin.guard';
import {PermissionString} from '../../../interfaces/Permission';

export const routes: Routes = [
    {path: '', loadChildren: '../admin-dashboard/admin-dashboard.module#AdminDashboardModule'},
    {path: 'system', loadChildren: '../admin-system/admin-system.module#AdminSystemModule'},
    {path: 'ticket-system', loadChildren: '../admin-ticket-system/admin-ticket-system.module#AdminTicketSystemModule'},
    {
        path        : 'user',
        canActivate : [AdminGuard],
        data        : {
            perm:
                [
                    PermissionString.admin_full_access,
                    PermissionString.user_manager_access,
                    PermissionString.group_manager_access
                ],
            acp : true
        },
        loadChildren: '../admin-user/admin-user.module#AdminUserModule'
    },
    {path: '**', redirectTo: ''}
];

export const AdminRouting: ModuleWithProviders = RouterModule.forChild(routes);
