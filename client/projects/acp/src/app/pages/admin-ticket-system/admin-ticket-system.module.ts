import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminTicketSystemComponent} from './admin-ticket-system.component';
import {AdminTicketSystemRouting} from './admin-ticket-system.routing';
import {SharedToolbarModule} from '../../modules/shared-toolbar.module';

@NgModule({
    declarations: [
        AdminTicketSystemComponent
    ],
    imports: [
        CommonModule,
        AdminTicketSystemRouting,
        SharedToolbarModule
    ]
})
export class AdminTicketSystemModule {}
