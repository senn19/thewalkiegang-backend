import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AdminGuard} from '../../../guards/core/admin.guard';
import {PermissionString} from '../../../interfaces/Permission';
import {AdminTicketSystemComponent} from './admin-ticket-system.component';

export const routes: Routes = [
    {
        path: '',
        component: AdminTicketSystemComponent,
        canActivate : [AdminGuard],
        data        : {perm: [PermissionString.ticket_system_access, PermissionString.admin_full_access], acp: true},
    }
];

export const AdminTicketSystemRouting: ModuleWithProviders = RouterModule.forChild(routes);
