import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';

@Component({
  selector: 'app-admin-ticket-system',
  templateUrl: './admin-ticket-system.component.html',
  styleUrls: ['./admin-ticket-system.component.scss']
})
export class AdminTicketSystemComponent implements OnInit {

  constructor(public lang: LanguageService) { }

  ngOnInit() {
  }

}
