import {AfterViewChecked, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {LanguageService} from '../../../services/LanguageManager/language.service';
import {AuthenticationService} from '../../../services/AuthManager/authentication.service';
import {UserPerms} from '../../../interfaces/User';
import {Subscription} from 'rxjs';
import {PermissionService} from '../../../services/PermManager/permission.service';
import {PermissionString} from '../../../interfaces/Permission';
import {LoginBoxStyle} from '../../includes/login-box/login-box.component';
import {LangSwitchStyle} from '../../includes/lang-switch/lang-switch.component';
import {ImageLoader} from '../../../utils/ImageLoader';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector   : 'app-home',
    templateUrl: './home.component.html',
    styleUrls  : ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewChecked, OnDestroy
{

    // Current user object
    currUser: UserPerms = new UserPerms();
    currUserSub: Subscription;

    // Anchor scrolling
    fragment: string;
    scrolledToFragment: string;

    LoginBoxStyle = LoginBoxStyle;
    LangSwitchStyle = LangSwitchStyle;

    icons: ImageLoader = new ImageLoader('icons');
    image: ImageLoader = new ImageLoader('images');

    constructor(public lang: LanguageService, private auth: AuthenticationService, private perm: PermissionService,
                private route: ActivatedRoute)
    {
    }

    ngOnInit(): void
    {
        if (this.auth.user)
        {
            this.currUserSub = this.perm.getPermissions().subscribe((user) =>
            {
                this.currUser = user;
            });
        }

        this.route.fragment.subscribe(fragment => this.fragment = fragment);
    }

    ngAfterViewChecked(): void
    {
        if (this.fragment && this.scrolledToFragment !== this.fragment)
        {
            document.querySelector('#' + this.fragment).scrollIntoView({behavior: 'smooth'});
            this.scrolledToFragment = this.fragment;
        }
    }

    ngOnDestroy(): void
    {
        if (this.currUserSub)
        {
            this.currUserSub.unsubscribe();
        }
    }

    checkAdminPermissions(): boolean
    {
        if (!this.currUser)
        {
            return false;
        }

        return this.perm.checkOnPermissions(this.currUser.permissions, [PermissionString.admin_access]);
    }

    @HostListener('window:scroll')
    onContainerScroll(): void
    {
        const element: HTMLElement = document.querySelector('.navbar');
        if (window.pageYOffset > element.clientHeight)
        {
            element.classList.add('navbar-inverse');
        }
        else
        {
            element.classList.remove('navbar-inverse');
        }
    }
}
