import {Component, OnDestroy, OnInit} from '@angular/core';
import {LanguageService} from '../../../../services/LanguageManager/language.service';
import {DatabaseService} from '../../../../services/DatabaseController/database.service';
import {Permission} from '../../../../interfaces/Permission';
import {Subscription} from 'rxjs';
import {Database} from '../../../../interfaces/Database';
import {TableMap} from '../../../includes/list-table/list-table.component';

@Component({
    selector   : 'app-admin-perms',
    templateUrl: './admin-perms.component.html',
    styleUrls  : ['./admin-perms.component.scss']
})
export class AdminPermsComponent implements OnInit, OnDestroy
{

    tableMap: TableMap[] = [
        {head: 'PAGE_ACP_PERMS_TABLE_NAME', property: 'name', lang: true},
        {head: 'PAGE_ACP_PERMS_TABLE_DESC', property: 'description', lang: true},
        {head: 'PAGE_ACP_PERMS_TABLE_RULE', property: 'permission'},
    ];

    pageSize = 20;

    permList: Permission[] = [];
    permListSub: Subscription;
    // DataBinding HTML-Template
    renderList: Permission[] = [];

    constructor(public lang: LanguageService, private db: DatabaseService) {
    }

    ngOnInit()
    {
        this.permListSub = this.db.observeItems<Permission>(Database.permissions).subscribe(
            (permissions: Permission[]) =>
            {
                this.permList = permissions;
                this.renderList = permissions;

                // alphabetical sort :)
                this.renderList.sort((a, b) => a.permission > b.permission ?
                    1 : a.permission < b.permission ? -1 : 0);
            }
        );
    }

    ngOnDestroy()
    {
        if (this.permListSub)
        {
            this.permListSub.unsubscribe();
        }
    }
}
