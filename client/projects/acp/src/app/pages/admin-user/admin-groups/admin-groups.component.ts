import {Component, OnDestroy, OnInit} from '@angular/core';
import {LanguageService} from '../../../../services/LanguageManager/language.service';
import {DatabaseService} from '../../../../services/DatabaseController/database.service';
import {Group} from '../../../../interfaces/Group';
import {Subscription} from 'rxjs';
import {Database} from '../../../../interfaces/Database';
import {OnTableEvent, TableEvent, TableEvents, TableMap} from '../../../includes/list-table/list-table.component';
import {Router} from '@angular/router';
import {SortProp, Utils} from '../../../../utils/Utils';
import {ListToolbarEvent, ListToolbarEvents, OnToolbarEvent} from '../../../includes/list-toolbar/list-toolbar.component';

@Component({
    selector   : 'app-admin-groups',
    templateUrl: './admin-groups.component.html',
    styleUrls  : ['./admin-groups.component.scss']
})
export class AdminGroupsComponent implements OnInit, OnDestroy, OnTableEvent<Group>, OnToolbarEvent
{
    // Table Data
    tableMap: TableMap[] = [
        {head: 'PAGE_ACP_GROUPS_TABLE_NAME', property: 'name'},
        {head: 'PAGE_ACP_GROUPS_TABLE_DESC', property: 'description'},
        {head: 'PAGE_ACP_GROUPS_TABLE_USER', property: 'user', count: true, pos: 'center'}
    ];

    pageSize = 20;

    groupList: Group[] = [];
    groupListSub: Subscription;
    // DataBinding HTML-Template
    renderList: Group[] = [];

    // Sort property by value - Memorization 
    sortProp: SortProp = {property: 'name', value: false};

    constructor(public lang: LanguageService, private db: DatabaseService, private router: Router)
    {
    }

    ngOnInit()
    {
        this.groupListSub = this.db.observeItems<Group>(Database.groups).subscribe(
            (groups: Group[]) =>
            {
                this.groupList = groups;
                this.renderList = groups;

                this.renderList.sort((a, b) =>
                    Utils.compareProp<Group>(a, b, this.sortProp.property, this.sortProp.value));
            }
        );
    }

    ngOnDestroy()
    {
        if (this.groupListSub)
        {
            this.groupListSub.unsubscribe();
        }
    }

    onTableEvent($event: TableEvent<any>): void
    {
        switch ($event.origin)
        {
            case TableEvents.trClick:
                this.router.navigate(['/acp/user/groups/form', ($event.value as Group).id]);
                break;
            case TableEvents.headClick:
                const prop: string = $event.value as string;
                this.sortProp.value = this.sortProp.property === prop ? !this.sortProp.value : false;
                this.sortProp.property = prop;
                this.renderList.sort((a, b) => Utils.compareProp<Group>(a, b, prop, this.sortProp.value, true));
                break;
        }
    }

    onToolbarEvent($event: ListToolbarEvent): void {
        if ($event.origin === ListToolbarEvents.createBtn)
        {
            this.router.navigate(['/acp/user/groups/form']);
        }
    }
}
