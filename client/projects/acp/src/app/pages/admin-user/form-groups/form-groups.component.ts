import {Component, OnInit} from '@angular/core';
import {FormToolbarEvent, FormToolbarEvents, OnToolbarEvent} from '../../../includes/form-toolbar/form-toolbar.component';
import {ActivatedRoute, Router} from '@angular/router';
import {DatabaseService} from '../../../../services/DatabaseController/database.service';
import {Database} from '../../../../interfaces/Database';
import {Group} from '../../../../interfaces/Group';
import {LanguageService} from '../../../../services/LanguageManager/language.service';
import {Observable, Subscription} from 'rxjs';
import {AlertService} from '../../../../services/AlertManager/alert.service';
import {Utils, ValidityRuleset} from '../../../../utils/Utils';
import {DocumentReference} from '@angular/fire/firestore';
import {User} from '../../../../interfaces/User';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {NgbTabChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import {Permission} from '../../../../interfaces/Permission';
import {ImageLoader} from '../../../../utils/ImageLoader';

interface MailList
{
    email: string;
    id: string;
}

interface LanguageString
{
    string: string;
    value?: any;
}

@Component({
    selector   : 'app-form-groups',
    templateUrl: './form-groups.component.html',
    styleUrls  : ['./form-groups.component.scss']
})
export class FormGroupsComponent implements OnInit, OnToolbarEvent
{
    currGroupId: string;
    currGroup: Group = new Group();
    currGroupSub: Subscription;

    mailList: MailList[] = [];
    permList: Permission[] = [];
    userListSub: Subscription;
    permListSub: Subscription;

    userSearchInput: string;
    andClose: boolean;
    unsavedChanges: boolean;
    currPerm: Permission;
    languageString: LanguageString;

    icons: ImageLoader = new ImageLoader('icons');

    // Filter Map after 2 characters, get levenshtein distance and return array
    emailSearch = (text: Observable<string>) =>
        text.pipe(debounceTime(200), distinctUntilChanged(),
            map((term: string) => term.length < 1 ? [] : this.filterMailList(this.mailList, term, 10)));


    constructor(private activeRoute: ActivatedRoute, private router: Router,
                private db: DatabaseService, public lang: LanguageService, private alert: AlertService) {}

    ngOnInit()
    {
        this.languageString = {string: 'PAGE_ACP_GROUPS_FORM_TITLE'};

        this.currGroupId = this.activeRoute.snapshot.paramMap.get('id');

        if (this.currGroupId)
        {
            this.retrieveGroupData();
        }

        this.retrieveUserList();
    }

    ngOnDestroy()
    {
        if (this.currGroupSub)
        {
            this.currGroupSub.unsubscribe();
        }

        if (this.userListSub)
        {
            this.userListSub.unsubscribe();
        }

        if (this.permListSub)
        {
            this.permListSub.unsubscribe();
        }
    }

    // Define actions on specific FormToolbarEvents
    onToolbarEvent($event: FormToolbarEvent): void
    {
        switch ($event.origin)
        {
            case FormToolbarEvents.saveForm:
                return this.save();
            case FormToolbarEvents.saveCloseForm:
                this.andClose = true;
                return this.save();
            case FormToolbarEvents.cancelFarm:
                this.router.navigate(['/acp/user/groups/']);
                break;
            default:
                return;
        }
    }

    // Retrieve user database data
    retrieveUserList(): void
    {
        this.userListSub = this.db.observeItems<User>(Database.user)
            .subscribe((user: User[]) =>
            {
                user.forEach((u: User) =>
                {
                    this.mailList.push({email: u.email, id: u.id});
                });
            });
    }

    // Retrieve group database data by provided id
    retrieveGroupData(): void
    {
        this.currGroupSub = this.db.getDocument<Group>(Database.groups, this.currGroupId).valueChanges()
            .subscribe((group: Group) =>
            {
                if (!group)
                {
                    this.router.navigate(['/acp/user/groups/']);
                    return this.alert.error(this.lang.get('ERROR_ADMIN_GROUPS_FORM_GROUP_DOES_NOT_EXIST'), 7);
                }

                this.currGroup = group;
                this.currGroup.id = this.currGroupId;

                this.languageString = {string: 'PAGE_ACP_GROUPS_FORM_EDIT_TITLE', value: this.currGroup.name};
            });
    }

    // Get email for given ID out of locally stored array
    getUserById(id: string): string
    {
        if (this.mailList.length === 0)
        {
            return;
        }

        return this.mailList.find((m: MailList) => m.id === id).email;
    }

    // Return if a group has a certain permission as boolean
    getPermissionValue(perm: Permission): boolean
    {
        return !!this.currGroup.permissions && !!this.currGroup.permissions.find(p => p.id === perm.id);
    }

    // Adds or removes permission to group
    changePermission(perm: Permission): void
    {
        const permOfGroup: Permission = this.currGroup && this.currGroup.permissions ? this.currGroup.permissions.find(
            (p: Permission) => p ? p.id === perm.id && p.permission === perm.permission : false) : null;
        const checkBox = document.querySelector<HTMLInputElement>(`#${perm.permission}`);
        this.currPerm = perm;

        if (checkBox.checked)
        {
            if (!permOfGroup)
            {
                return;
            }

            this.currGroup.permissions.splice(this.currGroup.permissions.indexOf(permOfGroup), 1);
        }
        else
        {
            if (permOfGroup)
            {
                return;
            }

            if (!this.currGroup.permissions)
            {
                this.currGroup.permissions = [];
            }

            this.currGroup.permissions.push(perm);
        }

        this.unsavedChanges = true;
    }

    // Adds an user to this group by provided email
    addUserToGroup(email: string): void
    {
        if (!email)
        {
            return;
        }

        const mail: MailList = this.mailList.find((m: MailList) => m.email === email);

        if (!mail)
        {
            return this.alert.error(this.lang.get('ERROR_ADMIN_GROUPS_FORM_ADD_USER_TO_GROUP', email), 7);
        }

        if (!this.currGroup.user)
        {
            this.currGroup.user = [];
        }

        if (this.currGroup.user.find((id: string) => id === mail.id))
        {
            // Clear input field
            this.userSearchInput = null;
            return this.alert.error(this.lang.get('ERROR_ADMIN_GROUPS_FORM_USER_ALREADY_IN_GROUP', email), 7);
        }

        this.unsavedChanges = true;
        this.currGroup.user.push(mail.id);
        this.alert.success(this.lang.get('SUCCESS_ADMIN_GROUPS_FORM_ADD_USER_TO_GROUP', email), 7);
        // Clear input field
        this.userSearchInput = null;
    }

    // Removes an user out of this group
    removeUser(id: string): void
    {
        this.unsavedChanges = true;
        this.currGroup.user.splice(this.currGroup.user.indexOf(id), 1);
        return this.alert.success(this.lang.get('SUCCESS_ADMIN_GROUPS_FORM_REMOVED_USER_FROM_GROUP', this.getUserById(id)), 7);
    }

    // Save or create a provided group
    save(): void
    {
        try
        {
            this.validateEntries();
        }
        catch (error)
        {
            return this.alert.error(this.lang.get(error.message), 7);
        }

        if (this.currGroup.id)
        {
            this.db.updateItem<Group>(Database.groups, this.currGroup)
                .then(() =>
                {
                    if (this.andClose)
                    {
                        this.router.navigate(['/acp/user/groups/']);
                    }

                    this.unsavedChanges = false;
                    this.alert.success(this.lang.get('SUCCESS_ADMIN_GROUPS_FORM_UPDATE', this.currGroup.name), 7);
                })
                .catch((error) => this.alert.error(error.message, 7));
        }
        else
        {
            this.db.addItem<Group>(Database.groups, this.currGroup)
                .then((ref: DocumentReference) =>
                {
                    if (this.andClose)
                    {
                        this.router.navigate(['/acp/user/groups/']);
                    }
                    else
                    {
                        this.router.navigate(['/acp/user/groups/form/', ref.id]);
                    }

                    this.unsavedChanges = false;
                    this.alert.success(this.lang.get('SUCCESS_ADMIN_GROUPS_FORM_CREATE', this.currGroup.name), 7);
                })
                .catch((error) => this.alert.error(error.message, 7));
        }
    }

    // Deletes group
    delete(): void
    {
        const groupName: string = document.querySelector<HTMLInputElement>('#user-group-delete-btn').value;

        if (!groupName)
        {
            return this.alert.error(this.lang.get('ERROR_ADMIN_GROUPS_FORM_DELETE_GROUP_NAME_MISSING', 7));
        }

        if (groupName !== this.currGroup.name)
        {
            return this.alert.error(this.lang.get('ERROR_ADMIN_GROUPS_FORM_DELETE_GROUP_NOT_EQUAL', 7));
        }

        this.router.navigate(['/acp/user/groups/']);
        this.db.deleteItem<Group>(Database.groups, this.currGroup)
            .then(() => this.alert.success(this.lang.get('SUCCESS_ADMIN_GROUPS_FORM_DELETE_GROUP', groupName), 7))
            .catch((error) => this.alert.error(this.lang.get(error.message), 7));
    }

    // Validate entries before saving them in database
    validateEntries(): void
    {
        if (!this.currGroup.name)
        {
            throw new Error('ERROR_ADMIN_GROUPS_FORM_MISSING_NAME');
        }

        if (!Utils.validateString(ValidityRuleset.alphabetical, this.currGroup.name))
        {
            throw new Error('ERROR_ADMIN_GROUPS_FORM_INVALID_NAME');
        }

        if (!this.currGroup.description)
        {
            throw new Error('ERROR_ADMIN_GROUPS_FORM_MISSING_DESC');
        }

        if (this.currGroup.permissions && this.currGroup.permissions.length > 0 && this.permList && this.permList.length > 0)
        {
            for (const perm of this.currGroup.permissions)
            {
                const permOfGroup: Permission = this.permList.find(p => p.permission === perm.permission);

                if (!permOfGroup)
                {
                    this.currGroup.permissions.splice(this.currGroup.permissions.indexOf(perm), 1);
                }
            }
        }
    }

    // Filters mailL
    filterMailList(mailList: MailList[], filter: string, maxEntries: number): string[]
    {
        const filteredList = [];

        for (const item of mailList)
        {
            if (item.email.split('@')[0].toLowerCase().indexOf(filter.toLowerCase()) >= 0 && filteredList.length <= maxEntries)
            {
                filteredList.push(item.email);
            }
        }

        return filteredList;
    }

    // Triggers event on (click)-event of perm tab
    onTabChange($event: NgbTabChangeEvent)
    {
        if ($event.nextId === 'user-group-form-perm')
        {
            if (!this.permListSub || this.permListSub.closed)
            {
                this.permListSub = this.db.observeItems<Permission>(Database.permissions)
                    .subscribe((permissions: Permission[]) =>
                        this.permList = permissions.sort((a, b) => a.permission > b.permission ? 1 :
                            a.permission < b.permission ? -1 : 0));
            }
        }
    }
}
