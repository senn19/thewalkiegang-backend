import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminProfilesComponent} from './admin-profiles/admin-profiles.component';
import {AdminPermsComponent} from './admin-perms/admin-perms.component';
import {AdminGroupsComponent} from './admin-groups/admin-groups.component';
import {SharedUiModule} from '../../modules/shared-ui.module';
import {AdminUserRouting} from './admin-user.routing';
import {SharedToolbarModule} from '../../modules/shared-toolbar.module';
import {FormProfilesComponent} from './form-profiles/form-profiles.component';
import {FormGroupsComponent} from './form-groups/form-groups.component';


@NgModule({
    declarations: [
        AdminProfilesComponent,
        AdminPermsComponent,
        AdminGroupsComponent,
        FormProfilesComponent,
        FormGroupsComponent
    ],
    imports     : [
        SharedToolbarModule,
        CommonModule,
        SharedUiModule,
        AdminUserRouting
    ]
})
export class AdminUserModule {}
