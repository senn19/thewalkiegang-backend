import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormToolbarEvent, FormToolbarEvents, OnToolbarEvent} from '../../../includes/form-toolbar/form-toolbar.component';
import {ActivatedRoute, Router} from '@angular/router';
import {DatabaseService} from '../../../../services/DatabaseController/database.service';
import {LanguageService} from '../../../../services/LanguageManager/language.service';
import {AlertService} from '../../../../services/AlertManager/alert.service';
import {User} from '../../../../interfaces/User';
import {Subscription} from 'rxjs';
import {Database} from '../../../../interfaces/Database';

interface LanguageString
{
    string: string;
    value?: any;
}

@Component({
    selector   : 'app-form-profiles',
    templateUrl: './form-profiles.component.html',
    styleUrls  : ['./form-profiles.component.scss']
})
export class FormProfilesComponent implements OnInit, OnDestroy, OnToolbarEvent
{
    // Current userId if router.url contains id
    currUserId: string;

    // Current user, will be filled with user, if userId has been provided
    currUser: User = new User();
    // Subscription of user
    currUserSub: Subscription;

    andClose: boolean;
    languageString: LanguageString;
    unsavedChanges: boolean;

    constructor(private activeRoute: ActivatedRoute, private router: Router,
                private db: DatabaseService, public lang: LanguageService, private alert: AlertService) { }

    ngOnInit()
    {
        this.languageString = {string: 'PAGE_ACP_PROFILES_FORM_TITLE'};

        this.currUserId = this.activeRoute.snapshot.paramMap.get('id');

        if (this.currUserId)
        {
            this.retrieveUserData();
        }
    }

    ngOnDestroy(): void
    {
        if (this.currUserSub)
        {
            this.currUserSub.unsubscribe();
        }
    }

    // Define actions on specific FormToolbarEvents
    onToolbarEvent($event: FormToolbarEvent): void
    {
        switch ($event.origin)
        {
            case FormToolbarEvents.saveForm:
                return this.save();
            case FormToolbarEvents.saveCloseForm:
                this.andClose = true;
                return this.save();
            case FormToolbarEvents.cancelFarm:
                this.router.navigate(['/acp/user/profiles/']);
                break;
            default:
                return;
        }
    }

    // Retrieve user database data
    retrieveUserData(): void
    {
        this.currUserSub = this.db.getDocument<User>(Database.user, this.currUserId).valueChanges()
            .subscribe((user: User) =>
            {
                if (!user)
                {
                    this.router.navigate(['/acp/user/profiles/']);
                    return this.alert.error(this.lang.get('ERROR_ADMIN_PROFILES_FORM_USER_DOES_NOT_EXIST'), 7);
                }

                this.currUser = user;
                this.currUser.id = this.currUserId;

                this.languageString = {string: 'PAGE_ACP_PROFILES_FORM_EDIT_TITLE', value: this.currUser.displayName};
            });
    }

    // Validate entries differently if user has been edited or created
    validateEntries(): void
    {
        if (!this.currUser.displayName)
        {
            throw Error('ERROR_ADMIN_PROFILES_FORM_MISSING_DISPLAY_NAME');
        }
    }

    // Validates and saves user in database
    save(): void
    {
        try
        {
            this.validateEntries();
        }
        catch (error)
        {
            return this.alert.error(this.lang.get(error.message), 7);
        }

        if (this.currUserId)
        {
            this.db.updateItem<User>(Database.user, this.currUser)
                .then(() =>
                {
                    if (this.andClose)
                    {
                        this.router.navigate(['/acp/user/profiles/']);
                    }

                    this.unsavedChanges = false;
                    this.alert.success(this.lang.get('SUCCESS_ADMIN_PROFILES_FORM_UPDATE', this.currUser.displayName), 7);
                });
        }
    }

    // Deletes user record out of database
    delete(): void
    {
        const email: string = document.querySelector<HTMLInputElement>('#user-profiles-delete-btn').value,
              displayName: string = this.currUser.displayName;

        if (!email)
        {
            return this.alert.error(this.lang.get('ERROR_ADMIN_PROFILES_FORM_DELETE_EMAIL_MISSING', 7));
        }

        if (email !== this.currUser.email)
        {
            return this.alert.error(this.lang.get('ERROR_ADMIN_PROFILES_FORM_DELETE_EMAIL_NOT_EQUAL', 7));
        }

        this.router.navigate(['/acp/user/profiles/']);
        this.db.deleteItem<User>(Database.user, this.currUser)
            .then(() => this.alert.success(this.lang.get('SUCCESS_ADMIN_PROFILES_FORM_DELETE_USER', displayName)))
            .catch((error) => this.alert.error(this.lang.get(error.message), 7));
    }
}
