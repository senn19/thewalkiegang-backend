import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AdminProfilesComponent} from './admin-profiles/admin-profiles.component';
import {AdminPermsComponent} from './admin-perms/admin-perms.component';
import {AdminGroupsComponent} from './admin-groups/admin-groups.component';
import {FormProfilesComponent} from './form-profiles/form-profiles.component';
import {FormGroupsComponent} from './form-groups/form-groups.component';
import {AdminGuard} from '../../../guards/core/admin.guard';
import {PermissionString} from '../../../interfaces/Permission';

export const routes: Routes = [
    {
        path       : 'profiles',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.admin_full_access, PermissionString.user_manager_access], acp: true},
        component  : AdminProfilesComponent
    },
    {
        path       : 'profiles/form/:id',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.admin_full_access, PermissionString.user_manager_access], acp: true},
        component  : FormProfilesComponent
    },
    {
        path     : 'perms',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.admin_full_access, PermissionString.group_manager_access], acp: true},
        component: AdminPermsComponent
    },
    {
        path       : 'groups',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.admin_full_access, PermissionString.group_manager_access], acp: true},
        component  : AdminGroupsComponent
    },
    {
        path       : 'groups/form',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.admin_full_access, PermissionString.group_manager_access], acp: true},
        component  : FormGroupsComponent
    },
    {
        path       : 'groups/form/:id',
        canActivate: [AdminGuard],
        data       : {perm: [PermissionString.admin_full_access, PermissionString.group_manager_access], acp: true},
        component  : FormGroupsComponent
    },
];

export const AdminUserRouting: ModuleWithProviders = RouterModule.forChild(routes);
