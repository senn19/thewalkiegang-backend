import {Component, OnDestroy, OnInit} from '@angular/core';
import {DatabaseService} from '../../../../services/DatabaseController/database.service';
import {User} from '../../../../interfaces/User';
import {Database} from '../../../../interfaces/Database';
import {LanguageService} from '../../../../services/LanguageManager/language.service';
import {Subscription} from 'rxjs';
import {OnTableEvent, TableEvent, TableEvents, TableMap} from '../../../includes/list-table/list-table.component';
import {AlertService} from '../../../../services/AlertManager/alert.service';
import {Router} from '@angular/router';
import {SortProp, Utils} from '../../../../utils/Utils';
import {
    ListToolbarEvent,
    ListToolbarEvents,
    OnToolbarEvent,
    ToolbarCustomHref
} from '../../../includes/list-toolbar/list-toolbar.component';
import {saveAs} from 'file-saver';

@Component({
    selector   : 'app-admin-profiles',
    templateUrl: './admin-profiles.component.html',
    styleUrls  : ['./admin-profiles.component.scss']
})
export class AdminProfilesComponent implements OnInit, OnDestroy, OnTableEvent<User>, OnToolbarEvent
{

    tableMap: TableMap[] = [
        {head: 'PAGE_ACP_PROFILES_TABLE_STATUS', property: 'disabled', invertedPin: true},
        {head: 'PAGE_ACP_PROFILES_TABLE_NAME', property: 'displayName'},
        {head: 'PAGE_ACP_PROFILES_TABLE_USERNAME', property: 'username'},
        {head: 'PAGE_ACP_PROFILES_TABLE_EMAIL', property: 'email'}
    ];

    pageSize = 20;

    userList: User[] = [];
    userListSub: Subscription;
    // DataBinding HTML-Template
    renderList: User[] = [];
    // Sort property by value - Memorization
    sortProp: SortProp = {property: 'displayName', value: false};

    // Custom href to firebase for user creation
    customHref: ToolbarCustomHref[] = [
        {
            text : 'PAGE_ACP_PROFILES_BUTTON_CREATE',
            href : 'https://console.firebase.google.com/u/0/project/thewalkiegang/authentication/users',
            class: 'btn-success'
        },
        {
            text : 'PAGE_ACP_PROFILES_BUTTON_DELETE',
            href : 'https://console.firebase.google.com/u/0/project/thewalkiegang/authentication/users',
            class: 'btn-danger'
        },
        {
            text : 'PAGE_ACP_PROFILES_BUTTON_DOWNLOAD',
            href : '',
            class: 'btn-info'
        }
    ];

    constructor(private db: DatabaseService, public lang: LanguageService, private alert: AlertService,
                private router: Router)
    {
    }

    ngOnInit()
    {
        this.userListSub = this.db.observeItems<User>(Database.user).subscribe(
            (user: User[]) =>
            {
                this.userList = user;
                this.renderList = user;

                // Sort userList in alphabetical order by displayName
                this.renderList.sort((a, b) => Utils.compareProp<User>(a, b, this.sortProp.property, this.sortProp.value));
            }
        );
    }

    ngOnDestroy()
    {
        if (this.userListSub)
        {
            this.userListSub.unsubscribe();
        }
    }

    // Catches TableEvents if they occur.
    onTableEvent($event: TableEvent<any>): void
    {
        switch ($event.origin)
        {
            case TableEvents.pinClick:
                this.changeUserStatus($event.value as User);
                break;
            case TableEvents.trClick:
                this.router.navigate(['/acp/user/profiles/form', ($event.value as User).id]);
                break;
            case TableEvents.headClick:
                const prop: string = $event.value as string;
                this.sortProp.value = this.sortProp.property === prop ? !this.sortProp.value : false;
                this.sortProp.property = prop;
                this.renderList.sort((a, b) => Utils.compareProp<User>(a, b, prop, this.sortProp.value));
                break;
        }
    }

    // Catches custom button and creates csv out of userList
    onToolbarEvent($event: ListToolbarEvent): void
    {
        if ($event.origin === ListToolbarEvents.customHref && $event.value === 2)
        {
            const csvArray: string = Utils.createCSV<User>(this.userList);
            saveAs(new Blob([csvArray], {type: 'text/html'}), 'userList.csv', {autoBom: true});
        }
    }

    changeUserStatus(currUser: User): void
    {
        // Check if currUser.disabled is undefined, user is enabled
        if (currUser.disabled === undefined)
        {
            currUser.disabled = false;
        }

        // Invert status and reassign to current user and store email locally
        currUser.disabled = !currUser.disabled;
        const email: string = currUser.email;

        // Remove all unnecessary values to avoid 'accidents'.
        delete currUser.email;
        delete currUser.username;
        delete currUser.displayName;

        // Update user in collection - security rule check only available for admins
        this.db.updateItem<User>(Database.user, currUser)
            .then(() => this.alert.success(
                this.lang.get(!currUser.disabled ? 'SUCCESS_ADMIN_PROFILES_ENABLED_USER' :
                    'SUCCESS_ADMIN_PROFILES_DISABLED_USER', email), 7))
            .catch((error) => this.alert.error(this.lang.get(error.message), 7));
    }
}
