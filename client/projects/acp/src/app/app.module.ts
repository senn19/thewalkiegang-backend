import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {LoginBoxComponent} from './includes/login-box/login-box.component';
import {AdminCpModule} from './pages/admin-cp/admin-cp.module';
import {SharedUiModule} from './modules/shared-ui.module';
import {SharedFireBaseModule} from './modules/shared-fireBase.module';
import {SiteNoticeComponent} from './pages/site-notice/site-notice.component';

@NgModule({
    declarations   : [
        AppComponent,
        HomeComponent,
        SiteNoticeComponent,
    ],
    imports        : [
        BrowserModule,
        AppRoutingModule,
        SharedFireBaseModule,
        SharedUiModule,
        AdminCpModule,
    ],
    providers      : [],
    bootstrap      : [AppComponent],
    entryComponents: [LoginBoxComponent]
})
export class AppModule {}
