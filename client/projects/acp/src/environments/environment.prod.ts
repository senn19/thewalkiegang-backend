export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyC8UwXse1kjFU3EXt8TBOltGGUjJ8nqS2o',
        authDomain: 'thewalkiegang.firebaseapp.com',
        databaseURL: 'https://thewalkiegang.firebaseio.com',
        projectId: 'thewalkiegang',
        storageBucket: 'thewalkiegang.appspot.com',
        messagingSenderId: '174630303781',
        appId: '1:174630303781:web:58b3294b498a60cc4ba7cc',
        measurementId: 'G-848CFT3BB4'
    }
};
