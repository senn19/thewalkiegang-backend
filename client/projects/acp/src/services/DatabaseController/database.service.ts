import {Injectable} from '@angular/core';
import {
    AngularFirestore,
    AngularFirestoreCollection,
    AngularFirestoreDocument,
    DocumentReference,
    Query,
    QuerySnapshot
} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {QueryStmt} from '../../interfaces/Database';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService
{

    constructor(private afs: AngularFirestore) { }

    /**
     * Adds an item to a specific collection provided by key. | Without ID management
     * @param {string} key - collection key
     * @param {T} item - which has to been added
     * @returns {Promise<DocumentReference>} - Promise with Document which have been added
     */
    addItem<T>(key: string, item: T): Promise<DocumentReference>
    {
        return this.afs.collection(key).add(Object.assign({}, this.prepareItem(item)));
    }

    /**
     * Updates an item in a specific collection provided by key.
     * @param {string} key - collection key
     * @param {T} item - which has to been updated
     * @param {boolean} overwrite - if true, updates entry destructively
     * @returns {Promise<void>} - void
     */
    updateItem<T>(key: string, item: T, overwrite: boolean = false): Promise<void>
    {
        const document: AngularFirestoreDocument = this.afs.collection(key).doc((item as any).id);
        const prepItem: any = this.prepareItem(item);
        return overwrite ? document.set(prepItem) : document.update(prepItem);
    }

    /**
     * Updates multiple documents in one operation. Use this if you need to update multiple documents.
     * It is not necessary to waste requests for single updates if you have to update multiple items.
     * @param {string} key - collection key
     * @param {T[]} items - which hast to been updated
     * @param {boolean} overwrite - if true, updates entry destructively
     * @returns {Promise<void>} - void
     */
    updateMultipleItems<T>(key: string, items: T[], overwrite: boolean = false): Promise<void>
    {
        const batch = this.afs.firestore.batch();

        for (const item of items)
        {
            const document: AngularFirestoreDocument = this.afs.collection(key).doc((item as any).id);
            const prepItem: any = this.prepareItem(item);
            overwrite ? batch.set(document.ref, prepItem) : batch.update(document.ref, prepItem);
        }

        return batch.commit();
    }

    /**
     * Deletes an item in a specific collection provided by key.
     * @param {string} key - collection key
     * @param {T} item - which has to been deleted
     * @param {string} id - you can provide only an id as string as well
     * @returns {Promise<void>} - void
     */
    deleteItem<T>(key: string, item: T = null, id: string = null): Promise<void>
    {
        return this.afs.collection(key).doc(id !== null ? id : (item as any).id).delete();
    }

    /**
     * Returns all items of a specific collection provided by key as an observable.
     * @param {string} key - collection key
     * @returns {Observable<T[]>} - Observable with realtime changes
     */
    observeItems<T>(key: string): Observable<T[]>
    {
        return this.afs.collection(key).snapshotChanges().pipe(
            map(action => action.map(a =>
            {
                const data = a.payload.doc.data(),
                      id   = a.payload.doc.id;
                return {id, ...data} as T;
            }))
        );
    }

    /**
     * Returns document for a provided key and id, both must be a string
     * @param {string} key - collection key
     * @param {string} id - id of a document you are looking for
     * @returns {AngularFirestoreDocument<T>} - Document
     */
    getDocument<T>(key: string, id: string): AngularFirestoreDocument<T>
    {
        return this.afs.collection(key).doc(id);
    }

    /**
     * Method for looking after items specified by provided parameters.
     * @param {string} key - it is possible to concat paths to key
     * @param {QueryStmt[]} where - array with where query stmts.
     * @param {{name, order}} orderBy - order query by provided values
     * @param {number} limit - limit query by provided number
     * @param {number} start - start query at
     * @param {number} end - end query at
     * @returns {Promise<any>} -
     */
    queryItems<T>(key: string, where: QueryStmt[], orderBy?: { name, order },
                  limit?: number, start?: number, end?: number): Promise<QuerySnapshot<T>>
    {
        if (where.length === 0)
        { return; }

        const collection: AngularFirestoreCollection = this.afs.collection(key);
        let collectionQuery: Query = collection.ref.where(where[0].key, where[0].opt, where[0].value);

        if (where.length > 1)
        {
            for (const stmt of where)
            {
                collectionQuery = collectionQuery.where(stmt.key, stmt.opt, stmt.value);
            }
        }

        if (orderBy)
        {
            collectionQuery = collectionQuery.orderBy(orderBy.name, orderBy.order ? orderBy.order : '');
        }

        if (limit)
        {
            collectionQuery = collectionQuery.limit(limit);
        }

        if (start)
        {
            collectionQuery = collectionQuery.startAt(start);
        }

        if (end)
        {
            collectionQuery = collectionQuery.endAt(end);
        }

        return collectionQuery.get() as Promise<QuerySnapshot<T>>;
    }

    /**
     * Prepares item which have to been added by removing id attributes recursively
     * Removes undefined attributes due to acceptance of firebase
     * @param {T} item - which will be inspected
     * @returns {any} plain javascript object
     */
    prepareItem<T>(item: T): any
    {
        if ((item as any).id !== undefined)
        {
            item = {...item as any};
            delete (item as any).id;
        }

        for (const key of Object.keys(item))
        {
            if (typeof item[key] === 'string' || typeof item[key] === 'number' || item[key] === null)
            {
                continue;
            }

            if (item[key] instanceof Array)
            {
                item[key] = this.prepareItem(item[key]);
            }

            if (item[key] === undefined)
            {
                delete item[key];
                continue;
            }

            if (item[key].id !== undefined)
            {
                item[key] = {...item[key] as any};
            }
        }

        return item;
    }
}
