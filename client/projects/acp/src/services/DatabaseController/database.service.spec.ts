import {TestBed} from '@angular/core/testing';

import {DatabaseService} from './database.service';
import {AngularFirestore} from '@angular/fire/firestore';

describe('DatabaseService', () =>
{
    class Test
    {
        constructor(public id: string, public testString: string, public testNumber: number, public testUndefined?) {}
    }

    class NestedTest
    {
        constructor(public id: string, public testArray: Test[], public testObj: Test) {}
    }

    let service: DatabaseService;
    const testObject: Test = new Test('test', 'test', 42);
    const nestedObject: NestedTest = new NestedTest('test', [testObject, testObject, testObject], testObject);


    beforeEach(() =>
    {
        TestBed.configureTestingModule(
            {
                providers: [
                    {provide: AngularFirestore, useValue: null}
                ]
            });

        service = TestBed.get(DatabaseService);
    });

    it('should be created', () =>
    {
        expect(service).toBeTruthy();
    });

    it('should generate plain javascript object', () =>
    {
        const obj = service.prepareItem(testObject);
        expect(obj.id).toBeUndefined();
        expect(obj.testString).toBe('test');
        expect(obj.testNumber).toBe(42);
        expect(obj.testUndefined).toBeUndefined();
        expect(obj instanceof Test).toBe(false);
    });

    it('should generate plain javascript object recursively', () =>
    {
        const obj = service.prepareItem(nestedObject);
        expect(obj.id).toBeUndefined();
        expect(obj.testArray.length).toBe(3);
        expect(obj.testArray[0].id).toBeDefined();
        expect(obj.testArray[0].testString).toBe('test');
        expect(obj.testArray[0].testNumber).toBe(42);
        expect(obj.testObj.id).toBeDefined();
        expect(obj.testObj.testString).toBe('test');
        expect(obj.testObj.testNumber).toBe(42);
    });
});
