import {Injectable} from '@angular/core';
import {Alert} from '../../interfaces/Alert';

@Injectable({
    providedIn: 'root'
})
export class AlertService
{
    /**
     * Contains all alerts which have been created over the time
     * @type {Alert[]} which have been created
     * @attribute simple will stop handling alerts via array if set to true
     */
    msg: Alert[] = [];
    simple: boolean;

    constructor() { }

    /**
     * Creates an error message with a given string
     * @param {string} msg - which should be displayed as an error message
     * @param {number} timeOut - if a message should be closed after certain time | time in s
     */
    error(msg: string, timeOut: number = 0): void
    {
        const alert = new Alert('danger', msg);
        this.create(alert, timeOut);
    }

    /**
     * Creates a success message with a given string
     * @param {string} msg - which should be displayed as a success message
     * @param {number} timeOut - if a message should be closed after certain time | time in s
     */
    success(msg: string, timeOut: number = 0): void
    {
        const alert = new Alert('success', msg);
        this.create(alert, timeOut);
    }

    /**
     * Creates an info message with a given string
     * @param {string} msg - which should be displayed as an info message
     * @param {number} timeOut - if a message should be closed after certain time | time in s
     */
    info(msg: string, timeOut: number = 0): void
    {
        const alert = new Alert('info', msg);
        this.create(alert, timeOut);
    }

    /**
     * Creates a warning message with a given string
     * @param {string} msg - which should be displayed as an warning message
     * @param {number} timeOut - if a message should be closed after certain time | time in s
     */
    warning(msg: string, timeOut: number = 0): void
    {
        const alert = new Alert('warning', msg);
        this.create(alert, timeOut);
    }

    /**
     * Creates an alert with prepared alert object and provided timeOut time
     * @param {Alert} alert - prepared alert object, u can customize your own alert
     * @param {number} timeOut - timeOut time in s
     */
    create(alert: Alert, timeOut: number): void
    {
        // If simple alerts is set to true and first element in alerts does exist
        if (this.simple && this.msg[0])
        {
            // If first element is equal to newly created, skip it, else reset timeOut
            if (this.msg[0].isEqual(alert))
            {
                return;
            }
            else
            {
                clearTimeout(this.msg[0].timeOut);
            }
        }

        if (timeOut > 0)
        {
            this.timeOut(alert, timeOut);
        }

        this.simple ? this.msg[0] = alert : this.msg.push(alert);
    }

    /**
     * Closes an alert and removes it from the alert array
     * @param {Alert} alert - which should be removed from the array | time in s
     */
    close(alert: Alert): void
    {
        const alertPos = this.msg.indexOf(alert);
        if (alertPos < 0) { return; }
        clearTimeout(this.msg[alertPos].timeOut);
        this.msg.splice(alertPos, 1);
    }

    /**
     * Invokes a close method after a provided number of time
     * @param {Alert} alert which have to been close
     * @param {number} timeOut which have to be in s and determines the time until close
     */
    timeOut(alert: Alert, timeOut: number): void
    {
        alert.timeOut = setTimeout(() =>
        {
            this.close(alert);
        }, timeOut * 1000);
    }
}
