import {TestBed} from '@angular/core/testing';

import {AlertService} from './alert.service';
import {Alert} from '../../interfaces/Alert';

describe('AlertService', () =>
{
    beforeEach(() => TestBed.configureTestingModule({
        providers: [AlertService]
    }));

    it('should be created', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        expect(service).toBeTruthy();
    });

    it('should create an error message', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.error('error');
        expect(service.msg[0].type).toBe('danger');
        expect(service.msg[0].message).toBe('error');
    });

    it('should create a success message', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.success('success');
        expect(service.msg[0].type).toBe('success');
        expect(service.msg[0].message).toBe('success');
    });

    it('should create an info message', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.info('info');
        expect(service.msg[0].type).toBe('info');
        expect(service.msg[0].message).toBe('info');
    });

    it('should create a warning message', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.warning('warning');
        expect(service.msg[0].type).toBe('warning');
        expect(service.msg[0].message).toBe('warning');
    });

    it('should create an error message and delete it', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.error('error');
        expect(service.msg.length).toBe(1);
        expect(service.msg[0].type).toBe('danger');
        expect(service.msg[0].message).toBe('error');
        service.close(service.msg[0]);
        expect(service.msg.length).toBe(0);
    });

    it('should create an error message (timeOut)', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.error('error', 1);
        expect(service.msg[0].type).toBe('danger');
        expect(service.msg[0].message).toBe('error');

        setTimeout(() =>
        {
            expect(service.msg.length).toBe(0);
        }, 2 * 1000);
    });

    it('should create a success message (timeOut)', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.success('success', 1);
        expect(service.msg[0].type).toBe('success');
        expect(service.msg[0].message).toBe('success');

        setTimeout(() =>
        {
            expect(service.msg.length).toBe(0);
        }, 2 * 1000);
    });

    it('should create an info message (timeOut)', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.info('info', 1);
        expect(service.msg[0].type).toBe('info');
        expect(service.msg[0].message).toBe('info');

        setTimeout(() =>
        {
            expect(service.msg.length).toBe(0);
        }, 2 * 1000);
    });

    it('should create a warning message (timeOut)', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.warning('warning', 1);
        expect(service.msg[0].type).toBe('warning');
        expect(service.msg[0].message).toBe('warning');

        setTimeout(() =>
        {
            expect(service.msg.length).toBe(0);
        }, 2 * 1000);
    });

    it('should create an error message which stays', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.error('error', 0);
        expect(service.msg[0].type).toBe('danger');
        expect(service.msg[0].message).toBe('error');

        setTimeout(() =>
        {
            expect(service.msg.length).toBe(1);
        }, 1000);
    });

    it('should create two alerts in simple mode', () =>
    {
        const service: AlertService = TestBed.get(AlertService);
        service.simple = true;
        service.create(new Alert('test', 'testing'), 4);
        expect(service.msg.length).toBe(1);
        service.success('success');
        expect(service.msg.length).toBe(1);
        expect(service.msg[0].type).toBe('success');
    });
});
