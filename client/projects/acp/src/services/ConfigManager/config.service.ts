import {Injectable} from '@angular/core';
import {DatabaseService} from '../DatabaseController/database.service';
import {Database, operator, QueryStmt} from '../../interfaces/Database';
import {Observable} from 'rxjs';
import {CfgIdentifiers, Config} from '../../interfaces/Config';
import {QuerySnapshot} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class ConfigService
{

    private configuration: Observable<Config<any>[]>;

    constructor(private db: DatabaseService)
    {
        this.loadConfiguration();
    }

    /**
     * Loads configuration out of database and stores the Observable in configuration attribute.
     * Therefore you only have to get the configuration attribute and subscribe on it.
     */
    private loadConfiguration(): void
    {
        this.configuration = this.db.observeItems<Config<any>>(Database.config);
    }

    /**
     * Returns a specific document queried by provided identifier
     * @param {CfgIdentifiers} identifier - to look for
     * @returns {Promise<QuerySnapshot<Config<any>>>} - configuration
     */
    getSpecificConfig(identifier: CfgIdentifiers): Promise<QuerySnapshot<Config<any>>>
    {
        return this.db.queryItems<Config<any>>(Database.config,
            [new QueryStmt('identifier', operator.EQ, identifier)]);
    }


    /**
     * Returns current configuration file as observable, which can be subscribed.
     * @returns {Observable<Config<any>[]>} - configuration file
     */
    getConfiguration(): Observable<Config<any>[]>
    {
        return this.configuration;
    }

    /**
     * Updates configuration file and returns a promise without any value
     * @param {Config<T>[]} config - provided configuration file
     * @returns {Promise<void>}
     */
    updateConfiguration<T>(config: Config<T>[]): Promise<void>
    {
        return this.db.updateMultipleItems<Config<T>>(Database.config, config);
    }

}
