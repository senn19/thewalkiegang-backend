import {TestBed} from '@angular/core/testing';

import {ConfigService} from './config.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {DatabaseService} from '../DatabaseController/database.service';

describe('ConfigService', () =>
{

    const authStub: any = {
        authState: jasmine.createSpyObj('authState', {
            pipe: null
        }),
        observeItems: jasmine.createSpy('observeItems').and.returnValue(null)
    };

    beforeEach(() => TestBed.configureTestingModule(
        {
            providers: [
                {provide: AngularFireAuth, useValue: authStub},
                {provide: AngularFirestore, useValue: {}},
                {provide: DatabaseService, useValue: authStub}
            ]
        })
    );

    it('should be created', () =>
    {
        const service: ConfigService = TestBed.get(ConfigService);
        expect(service).toBeTruthy();
    });
});
