import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Title} from '@angular/platform-browser';
import {LocalStorage} from '../../utils/LocalStorage';
import {sprintf} from 'sprintf-js';
import {ConfigService} from '../ConfigManager/config.service';
import {CfgIdentifiers, Config} from '../../interfaces/Config';
import {QuerySnapshot} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class LanguageService
{
    // Define default language and create empty Map on load
    public lang: string;
    langMap: Map<string, string> = new Map<string, string>();
    private langStorage = new LocalStorage('lang');

    // On service construction load language strings
    constructor(private http: HttpClient, private title: Title, private cfg: ConfigService)
    {
        const lang = this.langStorage.getItems();
        this.lang = lang ? lang : navigator.language.split('-')[0];
        this.loadLanguage();
    }

    /**
     * Method for changing language by provided string and reload language strings
     * @param {string} lang - language string
     */
    changeLanguage(lang: string): void
    {
        this.lang = lang;
        this.langStorage.setItems(lang);
        this.loadLanguage();
    }

    /**
     * Creates an observable and subscribes it, afterwards it fills
     * the language map attribute with all necessary key value pairs.
     * @param {string} title which will be set for page if key is provided.
     * @returns {Promise<void>} if necessary so
     */
    loadLanguage(title: string = null): void
    {
        this.http.get(`api/acp/language/${this.lang}`).toPromise()
            .then((vars) =>
            {
                this.langMap = new Map(Object.entries(vars));
                if (title)
                { this.title.setTitle(this.langMap.get(title)); }
            })
            .catch(() =>
            {
                // Fallback if browser language does not exist.
                this.cfg.getSpecificConfig(CfgIdentifiers.fallbackLanguage)
                    .then((docSnapshot: QuerySnapshot<Config<string>>) => this.changeLanguage(docSnapshot.docs[0].data().value));
            });
    }

    /**
     * Returns a specific value for a given key
     * @param {string} key - which have to been provided
     * @param args - providing args will allow to interpolate
     * @returns {string} value of the provided key
     */
    get(key: string, ...args): string
    {
        if (args.length > 0)
        {
            return sprintf(this.langMap.get(key), ...args) ? sprintf(this.langMap.get(key), ...args) : key;
        }

        return this.langMap.get(key) ? this.langMap.get(key) : key;
    }
}
