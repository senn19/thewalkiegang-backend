import {TestBed} from '@angular/core/testing';
import {LanguageService} from './language.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {DatabaseService} from '../DatabaseController/database.service';


describe('LanguageService', () =>
{
    const authStub: any = {
        authState: jasmine.createSpyObj('authState', {
            pipe: null
        }),
        observeItems: jasmine.createSpy('observeItems').and.returnValue(null)
    };
    
    beforeEach(() => TestBed.configureTestingModule(
        {
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                {provide: AngularFireAuth, useValue: authStub},
                {provide: AngularFirestore, useValue: {}},
                {provide: DatabaseService, useValue: authStub}
            ]
        }
    ));

    it('should be created', () =>
    {
        const service: LanguageService = TestBed.get(LanguageService);
        expect(service).toBeTruthy();
    });

    it('should return a string', () =>
    {
        // Create Service and a key, value pair, which will get mapped
        const service: LanguageService = TestBed.get(LanguageService),
              value                    = 'Testing this method.',
              key                      = 'LANGUAGE_TEST';

        service.langMap.set(key, value);
        expect(service.get(key)).toBe(value);
    });
});
