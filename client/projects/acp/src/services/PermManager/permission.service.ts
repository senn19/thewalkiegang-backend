import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {User, UserPerms} from '../../interfaces/User';
import {map, switchMap, take} from 'rxjs/operators';
import {Database, operator, QueryStmt} from '../../interfaces/Database';
import {QuerySnapshot} from '@angular/fire/firestore';
import {AuthenticationService} from '../AuthManager/authentication.service';
import {DatabaseService} from '../DatabaseController/database.service';
import {Permission, PermissionString} from '../../interfaces/Permission';

@Injectable({
    providedIn: 'root'
})
export class PermissionService
{

    private cachedUserPerms: UserPerms;
    cacheTimeOut: number = 1000 * 60 * 20;
    private cachedTimeOutRef: any;

    constructor(private auth: AuthenticationService, private db: DatabaseService) { }

    /**
     * Pipes through listenOnAuthState() Observable with logged in, checks for permissions for that user
     * and updates the userObject. Permissions will be stored in root as permissions[]. [user.permissions]
     * @returns {Observable<any>}
     */
    getPermissions(): Observable<UserPerms>
    {
        if (this.cachedUserPerms)
        {
            return of(this.cachedUserPerms);
        }

        return this.auth.listenOnAuthState().pipe(
            switchMap(async (user: User) =>
            {
                if (!user)
                {
                    return null;
                }

                // Get permission groups out of database and check if they contain logged in user, store permissions in user object
                return await this.db.queryItems<User>(Database.groups, [new QueryStmt('user', operator.AC, user.id)])
                    .then((querySnapshot: QuerySnapshot<User>) =>
                    {
                        if (querySnapshot.size !== 0)
                        {
                            querySnapshot.forEach((doc) =>
                            {
                                if (!(user as UserPerms).permissions)
                                {
                                    (user as UserPerms).permissions = [];
                                }

                                // Concat arrays and store permissions of all groups
                                (user as UserPerms).permissions = (user as UserPerms).permissions.concat(doc.data().permissions);
                            });
                        }

                        this.cachedUserPerms = user as UserPerms;

                        this.cachedTimeOutRef = setTimeout(() => this.cachedUserPerms = null, this.cacheTimeOut);

                        return user as UserPerms;
                    });
            })
        );
    }

    /**
     * Checks if a provided permission has been granted to user in specific group
     * @param {PermissionString[]} permission - which user should have
     * @returns {Observable<boolean>} - containing result
     */
    getObservablePermCheck(permission: PermissionString[]): Observable<boolean>
    {
        return this.getPermissions().pipe(
            take(1),
            map(
                (user: UserPerms) =>
                {
                    if (!user)
                    {
                        return false;
                    }

                    return this.checkOnPermissions(user.permissions, permission);
                }),
        );
    }

    /**
     * Iterates through an array of permissions and checks on provided permission
     * @param {Permission[]} permissions - array of permissions
     * @param {PermissionString[]} permissionStrings - permission string to check on
     * @returns {boolean} - result of check
     */
    checkOnPermissions(permissions: Permission[], permissionStrings: PermissionString[]): boolean
    {
        if (!permissions || permissions.length === 0)
        {
            return false;
        }

        if (permissionStrings.length === 1)
        {
            return !!permissions.find(p => p.permission === permissionStrings[0]);
        }

        for (const perm of permissions)
        {
            if (permissionStrings.find(p => p === perm.permission))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *  Clears current cached userPerms
     */
    clearCachedPerms(): void
    {
        if (this.cachedTimeOutRef)
        {
            clearTimeout(this.cachedTimeOutRef);
            this.cachedUserPerms = null;
        }
    }
}
