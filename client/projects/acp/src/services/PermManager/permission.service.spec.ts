import {TestBed} from '@angular/core/testing';
import {PermissionService} from './permission.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {DatabaseService} from '../DatabaseController/database.service';
import {Permission, PermissionString} from '../../interfaces/Permission';

describe('PermissionService', () =>
{
    let service: PermissionService;

    const examplePermission: Permission = new Permission('test', 'test', PermissionString.admin_access);
    const examplePermissions: Permission[] = [
        examplePermission,
        new Permission('test2', 'test2', PermissionString.user_manager_access),
        new Permission('test3', 'test3', null)
    ];
    const authStub: any = {
        authState: jasmine.createSpyObj('authState', {
            pipe: null
        }),
    };

    beforeEach(() =>
        {
            TestBed.configureTestingModule(
                {
                    providers: [
                        {provide: AngularFireAuth, useValue: authStub},
                        {provide: AngularFirestore, useValue: {}},
                        {provide: DatabaseService, useValue: {}}
                    ]
                });
            service = TestBed.get(PermissionService);
        }
    );

    it('should be created', () =>
    {
        expect(service).toBeTruthy();
    });

    it('should return false on no permissions', () =>
    {
        expect(service.checkOnPermissions(null, [PermissionString.admin_access])).toBe(false);
        expect(service.checkOnPermissions([], [PermissionString.admin_access])).toBe(false);
        expect(service.checkOnPermissions(undefined, [PermissionString.admin_access])).toBe(false);
        expect(service.checkOnPermissions(undefined, [])).toBe(false);
    });

    it('should return success on single permission check', () =>
    {
        expect(service.checkOnPermissions([examplePermission],
            [PermissionString.admin_access])).toBe(true);
        expect(service.checkOnPermissions([examplePermission],
            [PermissionString.user_manager_access])).toBe(false);
        expect(service.checkOnPermissions([examplePermission],
            [PermissionString.user_manager_access, PermissionString.admin_access])).toBe(true);
    });

    it('should return success on array permission check', () =>
    {
        expect(service.checkOnPermissions(examplePermissions, [PermissionString.admin_access])).toBe(true);
        expect(service.checkOnPermissions(examplePermissions, [PermissionString.system_access])).toBe(false);
        expect(service.checkOnPermissions(examplePermissions, [PermissionString.user_manager_access])).toBe(true);
    });
});
