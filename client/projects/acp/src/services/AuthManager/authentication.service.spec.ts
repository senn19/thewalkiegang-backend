import {TestBed} from '@angular/core/testing';
import {AuthenticationService} from './authentication.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {DatabaseService} from '../DatabaseController/database.service';
import {AngularFirestore} from '@angular/fire/firestore';

describe('AuthenticationService', () =>
{
    let service: AuthenticationService;

    const authStub: any = {
        authState: jasmine.createSpyObj('authState', {
            pipe: null
        }),
    };

    beforeEach(
        () =>
        {
            TestBed.configureTestingModule(
                {
                    providers: [
                        {provide: AngularFireAuth, useValue: authStub},
                        {provide: AngularFirestore, useValue: {}},
                        {provide: DatabaseService, useValue: {}}
                    ]
                });

            service = TestBed.get(AuthenticationService);
        }
    );

    it('should be created', () =>
    {
        expect(service).toBeTruthy();
    });

    it('should return default language string', () =>
    {
        expect(service.getCodeStmt('test')).toBe('ERROR_AUTH_GENERAL');
    });

});
