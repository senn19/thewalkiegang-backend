import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {auth} from 'firebase/app';
import {User} from '../../interfaces/User';
import {Observable, of} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {DatabaseService} from '../DatabaseController/database.service';
import {Database} from '../../interfaces/Database';
import {DocumentSnapshot} from '@angular/fire/firestore';
import UserCredential = firebase.auth.UserCredential;
import AuthError = firebase.auth.AuthError;
import AuthProvider = firebase.auth.AuthProvider;
import FirestoreError = firebase.firestore.FirestoreError;


@Injectable(
    {
        providedIn: 'root'
    }
)
export class AuthenticationService
{
    // Current logged in user or null | Observable for real time changes in document
    user: Observable<User>;

    constructor(public afAuth: AngularFireAuth, public db: DatabaseService)
    {
        this.user = this.listenOnAuthState();
    }

    /**
     * Pipes through authState and creates an Observable on logged in user document,
     * if a user is already logged in. It returns null if not.
     */
    listenOnAuthState(): Observable<User>
    {
        return this.afAuth.authState.pipe(
            switchMap((authState) =>
            {
                if (!authState)
                {
                    return of(null);
                }

                return this.db.getDocument<User>(Database.user, authState.uid)
                    .snapshotChanges().pipe(
                        map(a =>
                        {
                            const data = a.payload.data(),
                                  id   = a.payload.id;
                            return {id, ...data} as User;
                        })
                    );
            })
        );
    }

    /**
     * Updates displayName and username of an account and creates an entry if it doesn't exist
     * @param {firebase.auth.UserCredential} cred - UserCredential Firebase object
     * @returns {Promise<void>} - void
     */
    private updateAccountOnLogin(cred: UserCredential): Promise<void>
    {
        const user: User = new User(cred.user.email);

        // Create an instance of an user to update/create it in database
        user.displayName = cred.user.displayName ? cred.user.displayName : cred.user.email.split('@')[0];
        user.id = cred.user.uid;
        user.username = cred.additionalUserInfo.username;

        return this.db.getDocument<User>(Database.user, user.id).ref.get()
            .then((docSnapshot: DocumentSnapshot<User>) =>
            {

                // Delete attributes, because they already exists.
                if (docSnapshot.exists)
                {
                    user.displayName = undefined;
                    user.username = undefined;
                }

                return this.db.updateItem(Database.user, user, !docSnapshot.exists);
            });
    }

    /**
     * Abstract method for provider api calls by provided provider
     * @param {firebase.auth.AuthProvider} provider which have to been specified, f.e. google
     * @returns {Promise<void>} - void
     */
    private loginWithProvider(provider: AuthProvider): Promise<void>
    {
        return this.afAuth.auth.signInWithPopup(provider)
            .then((cred: UserCredential) =>
            {
                // Ignore this Promise for multi-threading.
                this.updateAccountOnLogin(cred);
            })
            .catch((error: FirestoreError) =>
            {
                throw new Error(this.getCodeStmt(error.code));
            });
    }

    /**
     * By invoking this method a user can login in via 'oAuthPopUp'
     * @returns {Promise<any>}
     */
    loginWithGoogle(): Promise<any>
    {
        const provider = new auth.GoogleAuthProvider();
        return this.loginWithProvider(provider);
    }

    /**
     * Authenticates a user, gets/updates his entry out of/in database and returns a promise
     * @param {User} user object with necessary information for login (email and password)
     * @returns {Promise<void>} - void
     * @throws Error: language string
     */
    loginWithEmailAndPassword(user: User): Promise<void>
    {
        if (!user.email || !user.password)
        {
            return Promise.reject(new Error(this.getCodeStmt('auth/argument-error')));
        }

        return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
            .then((cred: UserCredential) =>
            {
                // Ignore this promise for multi-threading.
                this.updateAccountOnLogin(cred);
            })
            .catch((error: AuthError) =>
            {
                throw new Error(this.getCodeStmt(error.code));
            });
    }

    /**
     * Signs out an already logged in user
     * @returns {Promise<string>} string with success message as language string
     * @throws Error: language string
     */
    logout(): Promise<string>
    {
        return this.afAuth.auth
            .signOut()
            .then(() =>
            {
                return this.getCodeStmt('auth/sign-out-success');
            })
            .catch(() =>
            {
                throw new Error(this.getCodeStmt('auth/sign-out-error'));
            });
    }

    /**
     * Decides on provided statusCode which language string have to be returned
     * @param {string} statusCode with the fireBase/custom provided status message
     * @returns {string} languageString
     */
    getCodeStmt(statusCode: string): string
    {
        switch (statusCode)
        {
            case 'auth/sign-out-success':
                return 'SUCCESS_AUTH_SIGN_OUT';
            case 'auth/argument-error':
                return 'ERROR_AUTH_INVALID_ARGS';
            case 'auth/invalid-email':
                return 'ERROR_AUTH_INVALID_EMAIL';
            case 'auth/user-disabled':
                return 'ERROR_AUTH_USER_DISABLED';
            case 'auth/user-not-found':
                return 'ERROR_AUTH_USER_NOT_FOUND';
            case 'auth/wrong-password':
                return 'ERROR_AUTH_WRONG_PASSWORD';
            case 'auth/sign-out-error':
                return 'ERROR_AUTH_SIGN_OUT';
            case 'auth/network-request-failed':
                return 'ERROR_AUTH_NETWORK_REQUEST_FAILED';
            default:
                return 'ERROR_AUTH_GENERAL';
        }
    }
}
