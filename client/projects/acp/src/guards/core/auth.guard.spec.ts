import {TestBed} from '@angular/core/testing';
import {AuthGuard} from './auth.guard';
import {AuthenticationService} from '../../services/AuthManager/authentication.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';

class MockRouter
{
}

describe('AuthGuard', () =>
{

    beforeEach(() =>
    {
        const authStub: any = {
            authState: jasmine.createSpyObj('authState', {
                pipe: null
            }),
        };

        TestBed.configureTestingModule({
            providers: [
                AuthGuard,
                {provide: AngularFireAuth, useValue: authStub},
                {provide: AngularFirestore, useValue: {}}
            ]
        });
    });

    it('should ...', () =>
    {
        const service = TestBed.get(AuthenticationService);
        const router: any = new MockRouter();
        const authGuard = new AuthGuard(service, router);

        expect(authGuard.canActivate).toBeTruthy();
    });
});
