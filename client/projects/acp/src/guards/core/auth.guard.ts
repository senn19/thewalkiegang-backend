import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../../services/AuthManager/authentication.service';
import {map, take, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate
{

    constructor(private auth: AuthenticationService, private router: Router) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean>
    {
        return this.auth.user.pipe(
            take(1),
            map(user => !!user),
            tap(loginState =>
            {
                if (!loginState)
                {
                    this.router.navigate(['home']);
                }
            })
        );
    }
}
