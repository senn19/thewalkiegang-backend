import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Meta, Title} from '@angular/platform-browser';
import {ConfigService} from '../../services/ConfigManager/config.service';
import {CfgIdentifiers, Config} from '../../interfaces/Config';
import {map, tap} from 'rxjs/operators';
import {PermissionService} from '../../services/PermManager/permission.service';

@Injectable({
    providedIn: 'root'
})
export class MetaGuard implements CanActivate
{
    constructor(private title: Title, private meta: Meta, private config: ConfigService, private perm: PermissionService) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean>
    {
        return this.config.getConfiguration().pipe(
            tap((config) =>
            {
                for (const cfg of config)
                {
                    if (cfg.value !== '' && cfg.value !== undefined)
                    {
                        this.reactOnIdentifier(cfg as Config<any>);
                    }
                }
            }),
            map(() => true)
        );
    }

    // react on provided configuration.
    reactOnIdentifier(config: Config<any>): void
    {
        switch (config.identifier)
        {
            case CfgIdentifiers.siteName:
                this.title.setTitle(config.value);
                break;
            case CfgIdentifiers.metaAuthor:
                this.meta.addTag({name: 'author', content: config.value});
                break;
            case CfgIdentifiers.metaDesc:
                this.meta.addTag({name: 'description', content: config.value});
                break;
            case CfgIdentifiers.metaKeywords:
                this.meta.addTag({name: 'keywords', content: config.value});
                break;
            case CfgIdentifiers.metaTitle:
                this.meta.addTag({name: 'title', content: config.value});
                break;
            case CfgIdentifiers.ogTitle:
                this.meta.addTag({name: 'og:title', content: config.value});
                break;
            case CfgIdentifiers.ogDesc:
                this.meta.addTag({name: 'og:description', content: config.value});
                break;
            case CfgIdentifiers.ogImage:
                this.meta.addTag({name: 'og:image', content: config.value});
                break;
            case CfgIdentifiers.ogUrl:
                this.meta.addTag({name: 'og:url', content: config.value});
                break;
            case CfgIdentifiers.permCacheTime:
                this.perm.cacheTimeOut = config.value * 60 * 1000;
                break;
        }
    }

}
