import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {take, tap} from 'rxjs/operators';
import {PermissionString} from '../../interfaces/Permission';
import {PermissionService} from '../../services/PermManager/permission.service';
import {AlertService} from '../../services/AlertManager/alert.service';
import {LanguageService} from '../../services/LanguageManager/language.service';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanActivate
{
    constructor(private perm: PermissionService, private router: Router, private alert: AlertService,
                private lang: LanguageService) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean>
    {

        const permStrings = next.data.perm as PermissionString[],
              acpLocation = next.data.acp as boolean;

        return this.perm.getObservablePermCheck(permStrings).pipe(
            take(1),
            tap(loginState =>
            {
                if (!loginState)
                {
                    if (acpLocation)
                    {
                        this.router.navigate(['acp']);
                    }
                    else
                    {
                        this.router.navigate(['home']);
                    }

                    this.alert.error(this.lang.get('ERROR_ROUTING_NO_PERMISSIONS'), 7);
                }
            })
        );
    }

}
