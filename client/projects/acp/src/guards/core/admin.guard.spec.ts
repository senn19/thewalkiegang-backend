import {TestBed} from '@angular/core/testing';

import {AdminGuard} from './admin.guard';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {PermissionService} from '../../services/PermManager/permission.service';
import {LanguageService} from '../../services/LanguageManager/language.service';
import {AlertService} from '../../services/AlertManager/alert.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {DatabaseService} from '../../services/DatabaseController/database.service';

class MockRouter
{
}

describe('AdminGuard', () =>
{
    const authStub: any = {
        authState: jasmine.createSpyObj('authState', {
            pipe: null
        }),
        observeItems: jasmine.createSpy('observeItems').and.returnValue(null)
    };

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [
                {provide: AngularFireAuth, useValue: authStub},
                {provide: AngularFirestore, useValue: {}},
                {provide: DatabaseService, useValue: authStub}
            ],
            imports: [
                HttpClientTestingModule,
            ],
        });
    });

    it('should ...', () =>
    {
        const lang = TestBed.get(LanguageService);
        const alert = TestBed.get(AlertService);
        const perm = TestBed.get(PermissionService);
        const router: any = new MockRouter();
        const adminGuard = new AdminGuard(perm, router, alert, lang);

        expect(adminGuard.canActivate).toBeTruthy();
    });
});
