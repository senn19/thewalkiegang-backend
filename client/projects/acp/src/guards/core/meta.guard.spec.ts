import {inject, TestBed} from '@angular/core/testing';
import {MetaGuard} from './meta.guard';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('MetaGuard', () =>
{
    const collectionStub: any = {
        pipe: jasmine.createSpy('pipe').and.returnValue(null)
    };
    const authStub: any = {
        authState : jasmine.createSpyObj('authState', {
            pipe: null
        }),
        collection: jasmine.createSpy('collection').and.returnValue({
            snapshotChanges: jasmine.createSpy(('snapshotChanges')).and.returnValue(collectionStub)
        })
    };

    beforeEach(() =>
    {
        TestBed.configureTestingModule({
            providers: [
                {provide: AngularFireAuth, useValue: authStub},
                {provide: AngularFirestore, useValue: authStub}
            ],
            imports  : [
                HttpClientTestingModule,
            ],
        });
    });

    it('should ...', inject([MetaGuard], (guard: MetaGuard) =>
    {
        expect(guard).toBeTruthy();
    }));
});
